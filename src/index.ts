import 'reflect-metadata';
import 'source-map-support/register';

// std
import * as http from 'http';

// 3p
import { Config, createApp } from '@foal/core';

// App
import * as express from 'express';
import { AppController } from './app/app.controller';
import { createDatabaseConnection } from './utils/db';

async function main() {
  if (Config.get('node.env') === 'test') {
    await createDatabaseConnection({ synchronize: true });
  } else {
    const connection = await createDatabaseConnection();
    await connection.runMigrations();
  }

  const expApp = express();

  if (Config.get('node.env') === 'development') {
    expApp.use((err, req, res, next) => {
      res.status(err.status || 500);
      res.send('error', {
        message: err.message,
        error: err,
      });
    });
  }

  const app = createApp(AppController, {}) as express.Application;

  const httpServer = http.createServer(app);
  const port = Config.get('port', 3001);

  httpServer.listen(port, () => {
    console.log(`Listening on port ${port}...`);
  });
}

main();
