import { Either, left, right } from 'fp-ts/lib/Either';
import { OAuth2Client } from 'google-auth-library';

import { Config, dependency } from '@foal/core';

import { LoggerService } from './logger';

interface GoogleUserDetails {
  name?: string;
  email?: string;
  picture?: string;
  isEmailVerified: boolean;
}
export class FirebaseService {
  @dependency
  logger: LoggerService;

  client: OAuth2Client;

  constructor() {
    this.client = new OAuth2Client(Config.get('firebase.client.id'));
  }

  async verify(token: string): Promise<Either<string, GoogleUserDetails>> {
    try {
      const ticket = await this.client.verifyIdToken({
        idToken: token,
        audience: Config.get('firebase.client.id'),
      });
      const payload = ticket.getPayload();

      if (payload) {
        this.logger.info(payload);
        const data: GoogleUserDetails = {
          name: payload.name,
          email: payload.email,
          isEmailVerified: !!payload.email_verified,
          picture: payload.picture,
        };

        return right(data);
      }

      return left('Invalid payload');
    } catch (error) {
      return left(error.message);
    }
  }
}
