import { getRepository } from 'typeorm';

import { TokenBlacklist } from '../../entities/user/token-blacklist.entity';

export const isTokenBlacklisted = async (token: string) =>
  !!(await getRepository(TokenBlacklist).find({ where: { token } })).length;

export const blacklistToken = async (token: string) =>
  await getRepository(TokenBlacklist).save({
    token,
  });
