import { Class, Hook, HookDecorator, HttpResponseBadRequest } from '@foal/core';
import { ClassTransformOptions, plainToClass } from 'class-transformer';

import { LoggerService } from '../services/utilities/logger';

export function UnserializeBody(
  cls: Class,
  options?: ClassTransformOptions,
): HookDecorator {
  return Hook(async ctx => {
    if (typeof ctx.request.body !== 'object' || ctx.request.body === null) {
      return new HttpResponseBadRequest(
        'The request body should be a valid JSON.',
      );
    }
    ctx.request.body = await plainToClass(cls, ctx.request.body, options);
  });
}

export function LogUserId() {
  return Hook((ctx, services) => {
    const logger = services.get(LoggerService);
    logger.info(`UserId is: ${ctx.user.id}`);
  });
}
