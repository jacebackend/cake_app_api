import {
  Hook,
  HookDecorator,
  HttpResponseForbidden,
  HttpResponseUnauthorized,
} from '@foal/core';

import { User } from '../entities';

export function PermissionRequired(perm: string): HookDecorator {
  return Hook(async (ctx, services) => {
    const currentUser = ctx.user as User;

    if (!currentUser) {
      return new HttpResponseUnauthorized({
        message: 'un-authorised',
      });
    }

    if (!(await currentUser.hasPerm(perm))) {
      return new HttpResponseForbidden({
        message: 'missing required permissions',
      });
    }
  });
}
