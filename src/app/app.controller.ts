import {
  Context,
  controller,
  Get,
  Hook,
  HttpResponseNoContent,
  HttpResponseNotFound,
  Options,
} from '@foal/core';

import { ApiController, OpenApiController } from './controllers';

@Hook(() => (ctx, services, response) => {
  response.setHeader('Access-Control-Allow-Origin', '*');
})
export class AppController {
  @Options('*')
  options(ctx: Context) {
    const response = new HttpResponseNoContent();
    response.setHeader(
      'Access-Control-Allow-Headers',
      'Content-Type,Authorization',
    );
    return response;
  }

  notFound() {
    return new HttpResponseNotFound({
      message: 'endpoint does not exist',
    });
  }

  @Get('*')
  get = () => this.notFound();

  // tslint:disable-next-line:member-ordering
  subControllers = [
    controller('/api/v1', ApiController),
    controller('/docs', OpenApiController),
  ];
}
