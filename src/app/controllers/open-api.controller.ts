import { SwaggerController } from '@foal/swagger';

import { ApiController } from './api.controller';

export class OpenApiController extends SwaggerController {
  options = { name: 'v1', controllerClass: ApiController, primary: true };
}
