// std
import { ok, strictEqual } from 'assert';

// 3p
import {
  Context,
  createController,
  getHttpMethod,
  getPath,
  isHttpResponseOK,
} from '@foal/core';
import { getRepository, Repository } from 'typeorm';

// App
import { createDatabaseConnection } from '../../../utils/db';
import { User } from '../../entities';
import { UserType } from '../../entities/user/interfaces';
import { UserController } from './user.controller';

describe('UserController', () => {
  let controller: UserController;
  let userRepo: Repository<User>;
  let connection;

  before(async () => {
    connection = await createDatabaseConnection({ synchronize: true });

    const user = new User();
    (user.email = 'test@email.com'),
      (user.username = 'test'),
      await user.setPassword('12345');
    user.userDetails = {
      kind: UserType.BasicUser,
      phonenumber: '0724000000',
    };

    userRepo = await getRepository(User);
    userRepo.insert(user);
  });

  after(async () => {
    await connection.synchronize(true);
    await connection.close();
  });

  describe('has a "login" method that', () => {
    it('should handle requests at POST /login.', () => {
      strictEqual(getHttpMethod(UserController, 'login'), 'POST');
      strictEqual(getPath(UserController, 'login'), '/login');
    });

    it('should return an HttpResponseOK on user login.', async () => {
      controller = createController(UserController);

      const ctx = new Context({
        body: {
          email: 'test@email.com',
          password: '12345',
        },
      });

      try {
        const result = await controller.login(ctx);
        ok(isHttpResponseOK(result));
      } catch (error) {
        throw new Error(error.message);
      }
    });
  });

  describe('has a "register" method that', () => {
    it('should handle requests at POST /register.', () => {
      strictEqual(getHttpMethod(UserController, 'register'), 'POST');
      strictEqual(getPath(UserController, 'register'), '/register');
    });

    it('should return an HttpResponseOK on user register.', async () => {
      controller = createController(UserController);

      const ctx = new Context({
        body: {
          username: 'test user',
          email: 'test@email.com',
          password: '12345',
          userDetails: {
            kind: 'basic-user',
            phonenumber: '0724000000',
          },
        },
      });

      try {
        const result = await controller.login(ctx);
        ok(isHttpResponseOK(result));
      } catch (error) {
        throw new Error(error.message);
      }
    });
  });
});
