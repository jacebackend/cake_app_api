import { classToPlain, plainToClass } from 'class-transformer';
import { validate } from 'class-validator';
import { decode } from 'jsonwebtoken';
import { getRepository } from 'typeorm';

import {
  Context,
  dependency,
  encryptPassword,
  Get,
  HttpResponseBadRequest,
  HttpResponseCreated,
  HttpResponseInternalServerError,
  HttpResponseMethodNotAllowed,
  HttpResponseNotFound,
  HttpResponseOK,
  HttpResponseUnauthorized,
  Post,
  Put,
  ServiceManager,
  ValidateBody,
  ValidateHeaders,
} from '@foal/core';
import { JWTRequired } from '@foal/jwt';
import { isCommon } from '@foal/password';

import {
  PasswordResetLinks,
  SignUpType,
  User,
  UserGroup,
  userRepository,
} from '../../entities';
import { UserGroups } from '../../entities/user/group.entity';
import { UserType } from '../../entities/user/interfaces';
import { userGroupRepository } from '../../entities/user/user-group.entity';
import { UnserializeBody } from '../../hooks';
import {
  FirebaseService,
  LoggerService,
  MailerService,
} from '../../services/utilities';
import {
  blacklistToken,
  isTokenBlacklisted,
} from '../../services/utilities/jwt';
import { headerSchema } from '../utils';
import { UserData } from './interfaces';

export class UserController {
  @dependency
  services: ServiceManager;
  firebase: FirebaseService;

  @Post('/login')
  @ValidateBody({
    additionalProperties: false,
    properties: {
      email: { type: 'string' },
      password: { type: 'string' },
    },
    required: ['email', 'password'],
    type: 'object',
  })
  async login(ctx: Context) {
    const { email, password } = ctx.request.body;
    const user = await getRepository(User).findOne({
      where: { email },
    });

    if (user) {
      return (await user.comparePassword(password))
        ? new HttpResponseOK({
            message: 'login successfull',
            data: {
              token: await user.generateToken(),
            },
          })
        : new HttpResponseNotFound({
            message:
              'user credentials provided dont match any existing account',
          });
    }

    return new HttpResponseNotFound({
      message: 'user credentials provided dont match any existing account',
    });
  }

  @Get('/verify/:token')
  async verifyUser(ctx: Context) {
    const { token } = ctx.request.params;
    const userData = decode(token, { complete: true }) as UserData;
    const {
      payload: { email },
    } = userData;
    if (email) {
      const user = await getRepository(User).findOne({
        where: { email },
      });

      if (user) {
        try {
          const userGroup = new UserGroup();
          userGroup.userId = user.id;
          userGroup.groupName = user.userType;

          user.userGroups = [userGroup];

          await userRepository().save(user);
        } catch (error) {
          return new HttpResponseInternalServerError({
            message: 'user verification failed',
            error,
          });
        }
      }
      return new HttpResponseOK({
        message: 'Account verified successfully',
      });
    }
  }

  @Post('/register')
  @ValidateBody({
    additionalProperties: false,
    properties: {
      username: { type: 'string' },
      email: { type: 'string' },
      password: { type: 'string' },
      userDetails: {
        kind: { type: 'string' },
        businessName: { type: 'string' },
        primaryAddressOfOperation: { type: 'string' },
        ownership: { type: 'string' },
        owners: {
          properties: {
            firstName: { type: 'string' },
            secondName: { type: 'string' },
            sirName: { type: 'string' },
            nationalId: { type: 'string' },
            phoneNumber: { type: 'string' },
          },
          type: 'array',
        },
        phonenumber: { type: 'string' },
      },
    },
    required: ['username', 'email', 'password', 'userDetails'],
    type: 'object',
  })
  @UnserializeBody(User)
  async register(ctx: Context) {
    const user: User = ctx.request.body;
    const errors = await validate(user);

    if (errors.length) {
      return new HttpResponseBadRequest({
        message: 'validation failed',
        errors,
      });
    }

    if (await isCommon(user.password)) {
      return new HttpResponseMethodNotAllowed({
        message: 'the password provided is too common',
      });
    }

    await user.setPassword(user.password);
    user.userType = user.userDetails.kind;

    try {
      const savedUser = await getRepository(User).save(user);

      const endpoint = `/api/v1/auth/verify/${await savedUser.generateToken()}`;
      const fullUrl =
        ctx.request.protocol + '://' + ctx.request.get('host') + endpoint;

      this.services.get(MailerService).sendMail({
        kind: 'confirmation-email',
        receiver: savedUser.email,
        confirmationUrl: fullUrl,
      });

      return new HttpResponseCreated({
        message:
          'registered successfully, check your email to verify this account',
        data: classToPlain(savedUser),
      });
    } catch (error) {
      return new HttpResponseInternalServerError({
        message: error.message,
      });
    }
  }

  @Post('/request-reset')
  @ValidateBody({
    additionalProperties: false,
    properties: {
      detail: { type: 'string' },
    },
    type: 'object',
  })
  async requestResetPassword(ctx: Context) {
    const { detail } = ctx.request.body;
    const user = await userRepository().findOne({
      where: [{ email: detail }, { username: detail }],
    });

    if (!user) {
      return new HttpResponseNotFound({
        message: 'user details provided do not exist',
      });
    }

    const passwordResetLink = new PasswordResetLinks();
    const passwordResetRepo = await getRepository(PasswordResetLinks);

    const otp = Math.floor((Math.random() / Math.random()) * 100000).toString();

    passwordResetLink.email = user.email;
    passwordResetLink.link = otp;

    const resetDetails = await passwordResetRepo.save(passwordResetLink);

    this.services.get(MailerService).sendMail({
      kind: 'password-reset',
      otp,
      receiver: user.email,
    });

    return new HttpResponseOK({
      message: 'password reset otp created',
      data: {
        email: resetDetails.email,
      },
    });
  }

  @Put('/reset-password')
  @ValidateBody({
    additionalProperties: false,
    properties: {
      email: { type: 'string' },
      password: { type: 'string' },
      otp: { type: 'string' },
    },
    type: 'object',
  })
  async resetPassword(ctx: Context) {
    const { email, otp, password } = ctx.request.body;
    const [passwordResetLinksRpo, userRepo] = await Promise.all([
      await getRepository(PasswordResetLinks),
      await getRepository(User),
    ]);

    const resetPasswordLink = await passwordResetLinksRpo.findOne({
      where: {
        email,
        link: otp,
        isValid: true,
      },
    });

    if (!resetPasswordLink) {
      return new HttpResponseNotFound({
        message: 'please provide valid password reset details',
      });
    }

    await Promise.all([
      await userRepo.update(
        { email },
        { password: await encryptPassword(password) },
      ),
      await passwordResetLinksRpo.update(
        { email, link: otp },
        { isValid: false },
      ),
    ]);

    return new HttpResponseOK({
      message: 'password reset successfully',
    });
  }

  @Post('/social-auth')
  @ValidateBody({
    additionalProperties: false,
    properties: {
      email: { type: 'string' },
      username: { type: 'string' },
    },
    required: ['email'],
    type: 'object',
  })
  async socialAuthentiaction(ctx: Context) {
    const { email, phonenumber } = ctx.request.body;
    const accountId = ctx.request.headers.accountid as string;

    console.log(ctx.request.headers);
    if (!accountId) {
      return new HttpResponseUnauthorized({
        message: 'please provide an account token',
      });
    }

    const result = await this.services.get(FirebaseService).verify(accountId);

    if (result._tag === 'Left') {
      return new HttpResponseUnauthorized({
        message: 'account token provided is invalid',
      });
    }

    const user = await userRepository().findOne({
      where: {
        email,
        signUpType: SignUpType.Social,
      },
    });

    if (user) {
      return new HttpResponseOK({
        message: 'login successfull',
        data: {
          token: await user.generateToken(),
        },
      });
    }

    try {
      let newUser = new User();

      newUser.email = email;
      newUser.isVerified = true;
      newUser.signUpType = SignUpType.Social;
      newUser.userDetails = { kind: UserType.BasicUser, phonenumber };

      newUser = await userRepository().save(newUser);

      await userGroupRepository().save(
        plainToClass(UserGroup, {
          userId: newUser.id,
          groupName: UserGroups.BasicUser,
        }),
      );

      return new HttpResponseOK({
        message: 'login successfull',
        data: {
          token: await newUser.generateToken(),
        },
      });
    } catch (error) {
      this.services.get(LoggerService).error(error);
      return new HttpResponseInternalServerError({
        message: error.message,
      });
    }
  }

  @JWTRequired({ blackList: isTokenBlacklisted })
  @ValidateHeaders(headerSchema)
  @Get('/logout')
  async logOut(ctx: Context) {
    try {
      await blacklistToken(ctx.request.headers.authorization!.split(' ')[1]);
      return new HttpResponseOK({
        message: 'user logout successful',
      });
    } catch (error) {
      return new HttpResponseInternalServerError({
        message: 'user logout failed',
      });
    }
  }
}
