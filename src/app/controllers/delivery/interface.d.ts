import {
  DeliveryAddress,
  DeliveryType,
} from '../../entities/delivery/interfaces';
export interface DeliveryRequest {
  address?: string;
  type: string;
  date: string;
}

export interface DeliveryParams {
  orderId: string;
}
