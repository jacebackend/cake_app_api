import { classToPlain, plainToClass } from 'class-transformer';
import { getRepository } from 'typeorm';

import {
  Context,
  Delete,
  Get,
  HttpResponseBadRequest,
  HttpResponseCreated,
  HttpResponseMethodNotAllowed,
  HttpResponseNoContent,
  HttpResponseNotFound,
  HttpResponseOK,
  Patch,
  Post,
  Put,
  ValidateBody,
  ValidateParams,
  ValidateQuery,
} from '@foal/core';
import { JWTRequired } from '@foal/jwt';
import { fetchUser } from '@foal/typeorm';

import {
  Delivery,
  deliveryRepository,
  orderRepository,
  User,
} from '../../entities';
import {
  DeliveryStatus,
  DeliveryType,
} from '../../entities/delivery/interfaces';
import { OrderStatus } from '../../entities/order/order.entity';
import { PermissionRequired } from '../../hooks';
import { Permissions } from '../permissions';
import { successResponse } from '../utils';
import { DeliveryParams, DeliveryRequest } from './interface';

const deliverySchema = {
  additionalProperties: false,
  properties: {
    address: { type: 'string' },
  },
  required: ['address'],
  type: 'object',
};

export class DeliveryController {
  @Get('/')
  @JWTRequired({ user: fetchUser(User) })
  @PermissionRequired(Permissions.GetDeliveries)
  @ValidateQuery({
    properties: {
      skip: { type: 'number' },
      take: { type: 'number' },
    },
    type: 'object',
  })
  async get(ctx: Context) {
    const deliverys = await deliveryRepository().find({
      skip: ctx.request.query.skip,
      take: ctx.request.query.take,
      order: {
        createdAt: 'ASC',
      },
    });

    return new HttpResponseOK({
      message: 'response successful',
      data: deliverys.map(delivery => classToPlain(delivery)),
    });
  }

  @Get('/:id')
  @JWTRequired({ user: fetchUser(User) })
  @PermissionRequired(Permissions.GetDeliveries)
  @ValidateParams({ properties: { id: { type: 'string' } }, type: 'object' })
  async getById(ctx: Context<User>) {
    const delivery = await deliveryRepository().findOne(ctx.request.params.id);

    if (!delivery || delivery.order.user.id !== ctx.user.id) {
      return new HttpResponseNotFound({
        message: 'delivery no found',
      });
    }

    return new HttpResponseOK(successResponse(delivery));
  }

  @Post('/')
  @JWTRequired({ user: fetchUser(User) })
  @PermissionRequired(Permissions.CreateDeliveries)
  @ValidateParams({
    properties: { orderId: { type: 'string' } },
    type: 'object',
  })
  @ValidateBody(deliverySchema)
  async post(ctx: Context<User>) {
    const { address, date, type } = ctx.request.body as DeliveryRequest;
    const { orderId } = ctx.request.params as DeliveryParams;

    const order = await orderRepository().findOne(orderId);

    if (!order || order.user.id !== ctx.user.id) {
      return new HttpResponseNotFound({
        message: 'order provided is not found',
      });
    }

    const delivery = plainToClass(Delivery, {
      order,
    });

    if (type === DeliveryType.Deliver) {
      if (!address) {
        return new HttpResponseBadRequest({
          message: 'please provide an address',
        });
      }

      delivery.address = JSON.parse(address);
      delivery.type = type;
    }

    delivery.date = new Date(date) || new Date();

    const [result] = await Promise.all([
      await deliveryRepository().save(delivery),
      await orderRepository().update(order.id, {
        status: OrderStatus.Delivery,
      }),
    ]);

    return new HttpResponseCreated(successResponse(result));
  }

  @Post('/:id')
  postById() {
    return new HttpResponseMethodNotAllowed();
  }

  @Patch('/')
  patch() {
    return new HttpResponseMethodNotAllowed();
  }

  @Put('/')
  put() {
    return new HttpResponseMethodNotAllowed();
  }

  @Put('/:id')
  @JWTRequired({ user: fetchUser(User) })
  @PermissionRequired(Permissions.UpdateDeliveries)
  @ValidateParams({ properties: { id: { type: 'string' } }, type: 'object' })
  @ValidateBody({
    address: { type: 'string' },
    status: { type: 'string' },
    required: [],
  })
  async putById(ctx: Context<User>) {
    const delivery = await deliveryRepository().findOne(ctx.request.params.id);

    if (!delivery || delivery.order.user.id !== ctx.user.id) {
      return new HttpResponseNotFound({
        message: 'delivery not found',
      });
    }

    if (ctx.request.body.status) {
      if (!Object.values(DeliveryStatus).includes(ctx.request.body.status)) {
        return new HttpResponseBadRequest({
          message: 'status provided is not valid',
        });
      }

      const order = await orderRepository().findOne(ctx.request.params.orderId);
      if (!order) {
        return new HttpResponseNotFound({
          message: 'order not found',
        });
      }

      if (ctx.request.body.status === DeliveryStatus.Completed) {
        await orderRepository().update(order.id, {
          status: OrderStatus.Fullfilled,
        });
      }
    }

    Object.assign(delivery, ctx.request.body);

    const result = await deliveryRepository().save(delivery);

    return new HttpResponseOK(successResponse(result));
  }

  @Delete('/')
  delete() {
    return new HttpResponseMethodNotAllowed();
  }

  @Delete('/:id')
  @JWTRequired({ user: fetchUser(User) })
  @PermissionRequired(Permissions.DeleteDeliveries)
  @ValidateParams({ properties: { id: { type: 'string' } }, type: 'object' })
  async deleteById(ctx: Context<User>) {
    const delivery = await getRepository(Delivery).findOne(
      ctx.request.params.id,
    );

    if (!delivery || delivery.order.user.id !== ctx.user.id) {
      return new HttpResponseNotFound({
        message: 'delivery not found',
      });
    }

    await deliveryRepository().delete(ctx.request.params.id);

    return new HttpResponseNoContent();
  }
}
