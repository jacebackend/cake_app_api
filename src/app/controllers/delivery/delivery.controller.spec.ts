// std
import {
  deepEqual,
  notDeepEqual,
  notStrictEqual,
  ok,
  strictEqual,
} from 'assert';

// 3p
import {
  Context,
  createController,
  getHttpMethod,
  getPath,
  isHttpResponseBadRequest,
  isHttpResponseCreated,
  isHttpResponseMethodNotAllowed,
  isHttpResponseNoContent,
  isHttpResponseNotFound,
  isHttpResponseOK,
} from '@foal/core';
import { Connection, getRepository } from 'typeorm';
import * as uuid from 'uuid';

// App
import { plainToClass } from 'class-transformer';
import { createDatabaseConnection } from '../../../utils/db';
import {
  Cake,
  CakeDesign,
  CakeToIngredients,
  Delivery,
  deliveryRepository,
  Ingredients,
  Order,
  OrderCake,
  orderCakeRepository,
  orderRepository,
  OrderStatus,
  User,
} from '../../entities';
import {
  DeliveryStatus,
  DeliveryType,
} from '../../entities/delivery/interfaces';
import {
  mockCake,
  mockCakeVariety,
  mockOrderCake,
  mockUser,
} from '../../tests';
import { DeliveryController } from './delivery.controller';

describe('DeliveryController', () => {
  let controller: DeliveryController;

  let deliveries: Delivery[];
  let orders: Order[];
  let cakes: Cake[];
  let users: User[];
  let cakeDesigns: CakeDesign[];

  let connection: Connection;

  before(async () => {
    connection = await createDatabaseConnection({ synchronize: true });
  });

  after(async () => {
    await connection.synchronize(true);
    await connection.close();
  });

  afterEach(async () => await connection.synchronize(true));

  beforeEach(async () => {
    controller = createController(DeliveryController);

    const ordersRepository = getRepository(Order);

    const cakeRepository = getRepository(Cake);
    const userRepository = getRepository(User);
    const ingredientsRepository = getRepository(Ingredients);
    const cakeIngredient = getRepository(CakeToIngredients);

    const [ingredients, savedUsers] = await Promise.all([
      ingredientsRepository.save(
        ['sample1', 'sample2', 'sample3'].map(name =>
          plainToClass(Ingredients, { name }),
        ),
      ),
      userRepository.save(
        [
          mockUser({}),
          mockUser({ username: 'test2', email: 'test2@email.com' }),
        ].map(user => plainToClass(User, user)),
      ),
    ]);

    await userRepository.save(savedUsers);

    cakeDesigns = await getRepository(CakeDesign).save(
      [mockCakeVariety({}), mockCakeVariety({ color: 'red' })].map(
        cakeVariety => plainToClass(CakeDesign, cakeVariety),
      ),
    );

    users = savedUsers;

    cakes = await cakeRepository.save([
      plainToClass(Cake, {
        cakeDesigns,
        ...mockCake({ name: 'Test cake', sellerId: users[0].id }),
      }),
      plainToClass(Cake, {
        cakeDesigns,
        ...mockCake({ name: 'Test cake2', sellerId: users[1].id }),
      }),
      plainToClass(Cake, {
        cakeDesigns,
        ...mockCake({ name: 'Test cake3', sellerId: users[1].id }),
      }),
    ]);

    cakeDesigns = await getRepository(CakeDesign).find();

    await cakeIngredient.save([
      plainToClass(CakeToIngredients, {
        cakeId: cakes[0].id,
        ingredientId: ingredients[0].id,
        quantity: '5',
      }),
      plainToClass(CakeToIngredients, {
        cakeId: cakes[0].id,
        ingredientId: ingredients[1].id,
        quantity: '5',
      }),
      plainToClass(CakeToIngredients, {
        cakeId: cakes[0].id,
        ingredientId: ingredients[2].id,
        quantity: '5',
      }),
      plainToClass(CakeToIngredients, {
        cakeId: cakes[1].id,
        ingredientId: ingredients[0].id,
        quantity: '5',
      }),
      plainToClass(CakeToIngredients, {
        cakeId: cakes[1].id,
        ingredientId: ingredients[1].id,
        quantity: '5',
      }),
      plainToClass(CakeToIngredients, {
        cakeId: cakes[1].id,
        ingredientId: ingredients[2].id,
        quantity: '5',
      }),
    ]);

    orders = await ordersRepository.save([
      plainToClass(Order, {
        user: users[0],
        status: OrderStatus.Checkout,
      }),
      plainToClass(Order, {
        user: users[1],
        status: OrderStatus.Checkout,
      }),
      plainToClass(Order, {
        user: users[0],
        status: OrderStatus.Checkout,
      }),
    ]);

    await orderCakeRepository().save([
      plainToClass(
        OrderCake,
        mockOrderCake({
          cakeVarietyId: cakeDesigns[0].id,
          orderId: orders[0].id,
        }),
      ),
      plainToClass(
        OrderCake,
        mockOrderCake({
          cakeVarietyId: cakeDesigns[1].id,
          orderId: orders[1].id,
        }),
      ),
      plainToClass(
        OrderCake,
        mockOrderCake({
          cakeVarietyId: cakeDesigns[1].id,
          orderId: orders[2].id,
        }),
      ),
    ]);

    const address1 = {
      address: 'Test address 1',
      latitude: '12',
      longitude: '12',
    };

    const address2 = {
      address: 'Test address 2',
      latitude: '12',
      longitude: '12',
    };

    const deliveryOne = plainToClass(Delivery, {
      address: address1,
      date: new Date('2019-1-2'),
    });

    const deliveryTwo = plainToClass(Delivery, {
      address: address2,
      date: new Date('2019-1-3'),
    });

    deliveryOne.order = orders[0];
    deliveryTwo.order = orders[1];

    deliveries = await deliveryRepository().save([deliveryOne, deliveryTwo]);
  });

  describe('has a "get" method that', () => {
    it('should handle requests at GET /.', () => {
      strictEqual(getHttpMethod(DeliveryController, 'get'), 'GET');
      strictEqual(getPath(DeliveryController, 'get'), '/');
    });

    it('should return an HttpResponseOK object with the delivery list.', async () => {
      const ctx = new Context({ query: {} });
      const response = await controller.get(ctx);

      if (!isHttpResponseOK(response)) {
        throw new Error(
          'The returned value should be an HttpResponseOK object.',
        );
      }

      if (!Array.isArray(response.body.data)) {
        throw new Error('The response body should be an array of deliverys.');
      }

      strictEqual(response.body.data.length, 2);
      ok(
        response.body.data.find(
          delivery =>
            delivery.address.address === deliveries[0].address!.address,
        ),
      );
      ok(
        response.body.data.find(
          delivery =>
            delivery.address.address === deliveries[1].address!.address,
        ),
      );
    });

    it('should support pagination', async () => {
      let ctx = new Context({
        query: {
          take: 1,
        },
      });
      const firsResponse = await controller.get(ctx);

      strictEqual(firsResponse.body.data.length, 1);

      ctx = new Context({
        query: {
          skip: 1,
        },
      });
      const secondResponse = await controller.get(ctx);

      strictEqual(secondResponse.body.data.length, 1);
      notDeepEqual(firsResponse.body.data[0], secondResponse.body.data[0]);
    });
  });

  describe('has a "getById" method that', () => {
    it('should handle requests at GET /:id.', () => {
      strictEqual(getHttpMethod(DeliveryController, 'getById'), 'GET');
      strictEqual(getPath(DeliveryController, 'getById'), '/:id');
    });

    it('should return an HttpResponseOK object if the delivery was found.', async () => {
      const ctx = new Context({
        params: {
          id: deliveries[1].id,
        },
      });
      const response = await controller.getById({
        ...ctx,
        user: users[1],
      });

      if (!isHttpResponseOK(response)) {
        throw new Error(
          'The returned value should be an HttpResponseOK object.',
        );
      }

      strictEqual(response.body.data.id, deliveries[1].id);
      deepEqual(response.body.data.address, deliveries[1].address);
    });

    it('should return an HttpResponseNotFound object if the delivery was not found.', async () => {
      const ctx = new Context({
        params: {
          id: uuid(),
        },
      });
      const response = await controller.getById(ctx);

      if (!isHttpResponseNotFound(response)) {
        throw new Error(
          'The returned value should be an HttpResponseNotFound object.',
        );
      }
    });
  });

  describe('has a "post" method that', () => {
    it('should handle requests at POST /.', () => {
      strictEqual(getHttpMethod(DeliveryController, 'post'), 'POST');
      strictEqual(getPath(DeliveryController, 'post'), '/');
    });

    it(
      'should create the delivery in the database and return it through ' +
        'an HttpResponseCreated object.',
      async () => {
        const address = {
          address: 'Test address 4',
          latitude: '12',
          longitude: '12',
        };

        const ctx = new Context({
          params: {
            orderId: orders[2].id,
          },
          body: {
            address: JSON.stringify(address),
            type: DeliveryType.Deliver,
            date: new Date('2019-1-1').toString(),
          },
        });

        strictEqual(orders[2].status, OrderStatus.Checkout);

        const response = await controller.post({
          ...ctx,
          user: users[0],
        });

        if (!isHttpResponseCreated(response)) {
          throw new Error(
            'The returned value should be an HttpResponseCreated object.',
          );
        }

        const delivery = await getRepository(Delivery).findOne({
          address,
        });
        const order = await orderRepository().findOne(orders[2].id);

        if (!delivery || !order) {
          throw new Error('No delivery or order  was found in the database.');
        }

        strictEqual(delivery.address!.address, address.address);
        strictEqual(order.status, OrderStatus.Delivery);

        strictEqual(response.body.data.id, delivery.id);
        strictEqual(
          response.body.data.address.address,
          delivery.address!.address,
        );
      },
    );

    it('should not create a deliver delivery without an address', async () => {
      const ctx = new Context({
        params: {
          orderId: orders[2].id,
        },
        body: {
          type: DeliveryType.Deliver,
          date: new Date('2019-1-1').toString(),
        },
      });
      const response = await controller.post({
        ...ctx,
        user: users[0],
      });

      ok(isHttpResponseBadRequest(response));
    });

    it('should create a pickup delivery', async () => {
      const ctx = new Context({
        params: {
          orderId: orders[2].id,
        },
        body: {
          type: DeliveryType.PickUp,
          date: new Date('2019-1-1').toString(),
        },
      });
      const response = await controller.post({
        ...ctx,
        user: users[0],
      });
      strictEqual(response.body.data.type, DeliveryType.PickUp);
    });
  });

  describe('has a "postById" method that', () => {
    it('should handle requests at POST /:id.', () => {
      strictEqual(getHttpMethod(DeliveryController, 'postById'), 'POST');
      strictEqual(getPath(DeliveryController, 'postById'), '/:id');
    });

    it('should return a HttpResponseMethodNotAllowed.', () => {
      ok(isHttpResponseMethodNotAllowed(controller.postById()));
    });
  });

  describe('has a "put" method that', () => {
    it('should handle requests at PUT /.', () => {
      strictEqual(getHttpMethod(DeliveryController, 'put'), 'PUT');
      strictEqual(getPath(DeliveryController, 'put'), '/');
    });

    it('should return a HttpResponseMethodNotAllowed.', () => {
      ok(isHttpResponseMethodNotAllowed(controller.put()));
    });
  });

  describe('has a "putById" method that', () => {
    it('should handle requests at PUT /:id.', () => {
      strictEqual(getHttpMethod(DeliveryController, 'putById'), 'PUT');
      strictEqual(getPath(DeliveryController, 'putById'), '/:id');
    });

    it('should update the delivery in the database and return it through an HttpResponseOK object.', async () => {
      const address = {
        address: 'some weird address',
        latitude: '12',
        longitude: '12',
      };

      const ctx = new Context({
        body: {
          address,
          status: DeliveryStatus.Completed,
        },
        params: {
          orderId: orders[0].id,
          id: deliveries[0].id,
        },
      });

      const response = await controller.putById({
        ...ctx,
        user: users[0],
      });

      if (!isHttpResponseOK(response)) {
        throw new Error(
          'The returned value should be an HttpResponseOK object.',
        );
      }

      const delivery = await getRepository(Delivery).findOne(deliveries[0].id);
      const order = await orderRepository().findOne(orders[0].id);

      if (!delivery || !order) {
        throw new Error();
      }

      strictEqual(delivery.address!.address, address.address);
      strictEqual(order.status, OrderStatus.Fullfilled);

      strictEqual(response.body.data.id, delivery.id);
      strictEqual(
        response.body.data.address.address,
        delivery.address!.address,
      );
    });

    it('should not update the other deliverys.', async () => {
      const ctx = new Context({
        body: {
          address: 'some weird address',
        },
        params: {
          orderId: orders[0].id,
          id: deliveries[0].id,
        },
      });
      await controller.putById({
        ...ctx,
        user: users[1],
      });

      const delivery = await getRepository(Delivery).findOne(deliveries[1].id);

      if (!delivery) {
        throw new Error();
      }

      notStrictEqual(delivery.address, 'some weird address');
    });

    it('should return an HttpResponseNotFound if the object does not exist.', async () => {
      const ctx = new Context({
        body: {
          address: 'changed address',
        },
        params: {
          orderId: orders[0].id,
          id: uuid(),
        },
      });
      const response = await controller.putById({
        ...ctx,
        user: users[1],
      });

      if (!isHttpResponseNotFound(response)) {
        throw new Error(
          'The returned value should be an HttpResponseNotFound object.',
        );
      }
    });

    it('should fail to update with invalid status', async () => {
      const ctx = new Context({
        body: {
          status: 'weird status',
        },
        params: {
          orderId: orders[0].id,
          id: deliveries[0].id,
        },
      });
      const response = await controller.putById({
        ...ctx,
        user: users[0],
      });

      if (!isHttpResponseBadRequest(response)) {
        throw new Error(
          'The returned value should be an isHttpResponseBadRequest object.',
        );
      }
    });
  });

  describe('has a "delete" method that', () => {
    it('should handle requests at DELETE /.', () => {
      strictEqual(getHttpMethod(DeliveryController, 'delete'), 'DELETE');
      strictEqual(getPath(DeliveryController, 'delete'), '/');
    });

    it('should return a HttpResponseMethodNotAllowed.', () => {
      ok(isHttpResponseMethodNotAllowed(controller.delete()));
    });
  });

  describe('has a "deleteById" method that', () => {
    it('should handle requests at DELETE /:id.', () => {
      strictEqual(getHttpMethod(DeliveryController, 'deleteById'), 'DELETE');
      strictEqual(getPath(DeliveryController, 'deleteById'), '/:id');
    });

    it('should delete the delivery and return an HttpResponseNoContent object.', async () => {
      const ctx = new Context({
        params: {
          orderId: orders[0].id,
          id: deliveries[0].id,
        },
      });
      const response = await controller.deleteById({
        ...ctx,
        user: users[0],
      });

      if (!isHttpResponseNoContent(response)) {
        throw new Error(
          'The returned value should be an HttpResponseNoContent object.',
        );
      }

      const delivery = await getRepository(Delivery).findOne(deliveries[0].id);

      strictEqual(delivery, undefined);
    });

    it('should not delete the other deliverys.', async () => {
      const ctx = new Context({
        params: {
          orderId: orders[0].id,
          id: deliveries[0].id,
        },
      });
      const response = await controller.deleteById({
        ...ctx,
        user: users[0],
      });

      if (!isHttpResponseNoContent(response)) {
        throw new Error(
          'The returned value should be an HttpResponseNoContent object.',
        );
      }

      const delivery = await getRepository(Delivery).findOne(deliveries[1].id);

      notStrictEqual(delivery, undefined);
    });

    it('should return an HttpResponseNotFound if the delivery was not fond.', async () => {
      const ctx = new Context({
        params: {
          orderId: orders[0].id,
          id: uuid(),
        },
      });
      const response = await controller.deleteById({
        ...ctx,
        user: users[1],
      });

      if (!isHttpResponseNotFound(response)) {
        throw new Error(
          'The returned value should be an HttpResponseNotFound object.',
        );
      }
    });
  });
});
