export enum Permissions {
  // cakes
  CreateCakes = 'create-cakes',
  UpdateCakes = 'update-cakes',
  DeleteCakes = 'delete-cakes',
  ViewCakes = 'view-cakes',
  GetCake = 'get-cakes',

  // orders
  CreateOrders = 'create-orders',
  UpdateOrders = 'update-orders',
  DeleteOrders = 'delete-orders',
  ViewOrders = 'view-orders',
  GetOrder = 'get-orders',

  // deliveries
  CreateDeliveries = 'create-deliveries',
  UpdateDeliveries = 'update-deliveries',
  DeleteDeliveries = 'delete-deliveries',
  ViewDeliveries = 'view-deliveries',
  GetDeliveries = 'get-deliveries',
}
