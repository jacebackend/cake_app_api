export const cakeSchema = {
  additionalProperties: false,
  properties: {
    name: { type: 'string', maxLength: 20 },
    ingredients: {
      properties: {
        name: { type: 'string' },
        quantity: { type: 'string' },
      },
      type: 'array',
      required: ['name', 'quantity'],
    },
    varieties: {
      properties: {
        color: { type: 'string' },
        design: { type: 'string' },
        price: {
          properties: {
            unit: { type: 'string' },
            amount: { type: 'number' },
          },
          type: 'object',
        },
        topings: {
          properties: {
            name: { type: 'string' },
            price: { type: 'number' },
          },
          type: 'array',
        },
        images: {
          properties: {
            url: { type: 'string' },
          },
          type: 'array',
        },
      },
      type: 'array',
      required: ['color', 'design', 'price'],
    },
  },
  required: ['name', 'ingredients', 'varieties'],
  type: 'object',
};
