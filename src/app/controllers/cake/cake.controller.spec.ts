// std
import { notDeepEqual, notStrictEqual, ok, strictEqual } from 'assert';

// 3p
import {
  Context,
  createController,
  getHttpMethod,
  getPath,
  isHttpResponseCreated,
  isHttpResponseMethodNotAllowed,
  isHttpResponseNotFound,
  isHttpResponseOK,
} from '@foal/core';
import { Connection, getRepository } from 'typeorm';

// App
import { plainToClass } from 'class-transformer';
import { createDatabaseConnection } from '../../../utils/db';
import { Cake } from '../../entities';
import { CakeDesign } from '../../entities/cake/cake-design.entity';
import { CakeToIngredients } from '../../entities/cake/cake-to-ingredients.entity';
import { Ingredients } from '../../entities/cake/ingredients.entity';
import { User } from '../../entities/user/user.entity';
import {
  getMockCakeRequestBody,
  mockCake,
  mockCakeVariety,
  mockUser,
} from '../../tests';
import { CakeController } from './cake.controller';

describe('CakeController', () => {
  let controller: CakeController;
  let cakes: Cake[];
  let users: User[];
  let token: string;
  let connection: Connection;

  before(async () => {
    connection = await createDatabaseConnection({ synchronize: true });
  });

  after(async () => {
    await connection.synchronize(true);
    await connection.close();
  });

  afterEach(async () => await connection.synchronize(true));

  beforeEach(async () => {
    controller = createController(CakeController);

    const cakeRepository = getRepository(Cake);
    const userRepository = getRepository(User);
    const ingredientsRepository = getRepository(Ingredients);
    const cakeIngredient = getRepository(CakeToIngredients);

    const [ingredients, savedUsers] = await Promise.all([
      ingredientsRepository.save(
        ['floor', 'sugar', 'everything nice'].map(name =>
          plainToClass(Ingredients, { name }),
        ),
      ),
      userRepository.save(
        [
          mockUser({}),
          mockUser({ username: 'test2', email: 'test2@email.com' }),
        ].map(user => plainToClass(User, user)),
      ),
    ]);

    await userRepository.save(savedUsers);

    const cakeVarieties = [
      mockCakeVariety({}),
      mockCakeVariety({ color: 'red' }),
    ].map(cakeVariety => plainToClass(CakeDesign, cakeVariety));
    users = savedUsers;

    cakes = await cakeRepository.save([
      plainToClass(Cake, {
        cakeVarieties,
        ...mockCake({ name: 'Test cake', sellerId: users[0].id }),
      }),
      plainToClass(Cake, {
        cakeVarieties,
        ...mockCake({ name: 'Test cake2', sellerId: users[1].id }),
      }),
    ]);

    await cakeIngredient.save([
      plainToClass(CakeToIngredients, {
        cakeId: cakes[0].id,
        ingredientId: ingredients[0].id,
        quantity: '5',
      }),
      plainToClass(CakeToIngredients, {
        cakeId: cakes[0].id,
        ingredientId: ingredients[1].id,
        quantity: '5',
      }),
      plainToClass(CakeToIngredients, {
        cakeId: cakes[0].id,
        ingredientId: ingredients[2].id,
        quantity: '5',
      }),
      plainToClass(CakeToIngredients, {
        cakeId: cakes[1].id,
        ingredientId: ingredients[0].id,
        quantity: '5',
      }),
      plainToClass(CakeToIngredients, {
        cakeId: cakes[1].id,
        ingredientId: ingredients[1].id,
        quantity: '5',
      }),
      plainToClass(CakeToIngredients, {
        cakeId: cakes[1].id,
        ingredientId: ingredients[2].id,
        quantity: '5',
      }),
    ]);

    token = await users[0].generateToken();
  });

  describe('has a "get" method that', () => {
    it('should handle requests at GET /.', () => {
      strictEqual(getHttpMethod(CakeController, 'get'), 'GET');
      strictEqual(getPath(CakeController, 'get'), '/');
    });

    it('should return an HttpResponseOK object with the cake list.', async () => {
      const ctx = new Context({
        query: {},
      });
      const response = await controller.get(ctx);

      if (!isHttpResponseOK(response)) {
        throw new Error(
          'The returned value should be an HttpResponseOK object.',
        );
      }

      if (!Array.isArray(response.body.data)) {
        throw new Error('The response body should be an array of cakes.');
      }

      strictEqual(response.body.data.length, 2);
      ok(response.body.data.find(cake => cake.name === cakes[0].name));
      ok(response.body.data.find(cake => cake.name === cakes[1].name));
    });

    it('should support pagination', async () => {
      let ctx = new Context({
        query: {
          take: 1,
        },
      });
      const firsResponse = await controller.get(ctx);

      strictEqual(firsResponse.body.data.length, 1);

      ctx = new Context({
        query: {
          skip: 1,
        },
      });
      const secondResponse = await controller.get(ctx);

      strictEqual(secondResponse.body.data.length, 1);
      notDeepEqual(firsResponse.body.data[0], secondResponse.body.data[0]);
    });
  });

  describe('has a "getById" method that', async () => {
    it('should handle requests at GET /:id.', () => {
      strictEqual(getHttpMethod(CakeController, 'getById'), 'GET');
      strictEqual(getPath(CakeController, 'getById'), '/:id');
    });

    it('should return an HttpResponseOK object if the cake was found.', async () => {
      const ctx = new Context({
        headers: {
          authorization: `Bearer ${token}`,
        },
        params: {
          id: cakes[0].id,
        },
      });
      const response = await controller.getById(ctx);

      if (!isHttpResponseOK(response)) {
        throw new Error(
          'The returned value should be an HttpResponseOK object.',
        );
      }

      strictEqual(response.body.data.id, cakes[0].id);
      strictEqual(response.body.data.name, cakes[0].name);
    });

    it('should return an HttpResponseNotFound object if the cake was not found.', async () => {
      const ctx = new Context({
        params: {
          id: users[0].id,
        },
      });
      const response = await controller.getById(ctx);

      if (!isHttpResponseNotFound(response)) {
        throw new Error(
          'The returned value should be an HttpResponseNotFound object.',
        );
      }
    });
  });

  describe('has a "getByMerchant" method that', async () => {
    it('should handle requests at GET /merchant/:id.', () => {
      strictEqual(getHttpMethod(CakeController, 'getMerchantCakes'), 'GET');
      strictEqual(getPath(CakeController, 'getMerchantCakes'), 'merchant/:id');
    });

    it('should return an HttpResponseOK object if the merchants cakes were found.', async () => {
      const ctx = new Context({
        params: {
          id: users[0].id,
        },
      });
      const response = await controller.getMerchantCakes(ctx);

      if (!isHttpResponseOK(response)) {
        throw new Error(
          'The returned value should be an HttpResponseOK object.',
        );
      }

      strictEqual(response.body.data[0].id, cakes[0].id);
      strictEqual(response.body.data[0].name, cakes[0].name);
      strictEqual(response.body.data[0].sellerId, users[0].id);
    });

    it('should return an HttpResponseNotFound object if the merchant was not found.', async () => {
      const ctx = new Context({
        params: {
          id: cakes[0].id,
        },
      });
      const response = await controller.getMerchantCakes(ctx);

      if (!isHttpResponseNotFound(response)) {
        throw new Error(
          'The returned value should be an HttpResponseNotFound object.',
        );
      }
    });
  });

  describe('has a "post" method that', () => {
    it('should handle requests at POST /.', () => {
      strictEqual(getHttpMethod(CakeController, 'post'), 'POST');
      strictEqual(getPath(CakeController, 'post'), '/');
    });

    it(
      'should create the cake in the database and return it through ' +
        'an HttpResponseCreated object.',
      async () => {
        const ctx = new Context({
          user: users[0],
          body: getMockCakeRequestBody(),
        });

        const fullContext = {
          ...ctx,
          user: users[0],
        };

        const response = await controller.post(fullContext);

        if (!isHttpResponseCreated(response)) {
          throw new Error(
            'The returned value should be an HttpResponseCreated object.',
          );
        }

        const cake = await getRepository(Cake).findOne({
          where: { name: 'Red velvet' },
        });

        if (!cake) {
          throw new Error('No Red velvet was found in the database.');
        }

        strictEqual(cake.name, 'Red velvet');

        strictEqual(response.body.data.id, cake.id);
        strictEqual(response.body.data.name, cake.name);
      },
    );
  });

  describe('has a "postById" method that', () => {
    it('should handle requests at POST /:id.', () => {
      strictEqual(getHttpMethod(CakeController, 'postById'), 'POST');
      strictEqual(getPath(CakeController, 'postById'), '/:id');
    });

    it('should return a HttpResponseMethodNotAllowed.', () => {
      ok(isHttpResponseMethodNotAllowed(controller.postById()));
    });
  });

  describe('has a "patch" method that', () => {
    it('should handle requests at PATCH /.', () => {
      strictEqual(getHttpMethod(CakeController, 'patch'), 'PATCH');
      strictEqual(getPath(CakeController, 'patch'), '/');
    });

    it('should return a HttpResponseMethodNotAllowed.', () => {
      ok(isHttpResponseMethodNotAllowed(controller.patch()));
    });
  });

  describe('has a "patchById" method that', () => {
    it('should handle requests at PATCH /:id.', () => {
      strictEqual(getHttpMethod(CakeController, 'patchById'), 'PATCH');
      strictEqual(getPath(CakeController, 'patchById'), '/:id');
    });

    it('should update the cake in the database and return it through an HttpResponseOK object.', async () => {
      const ctx = new Context({
        body: {
          name: 'Black Forest',
        },
        params: {
          id: cakes[0].id,
        },
      });

      const fullContext = {
        ...ctx,
        user: users[0],
      };

      const response = await controller.patchById(fullContext);

      if (!isHttpResponseOK(response)) {
        throw new Error(
          'The returned value should be an HttpResponseOK object.',
        );
      }

      const cake = await getRepository(Cake).findOne(cakes[0].id);

      if (!cake) {
        throw new Error();
      }

      strictEqual(cake.name, 'Black Forest');

      strictEqual(response.body.data.id, cake.id);
      strictEqual(response.body.data.name, 'Black Forest');
    });

    it('should not update the other cakes.', async () => {
      const ctx = new Context({
        body: {
          name: 'Black Forest',
        },
        params: {
          id: cakes[0].id,
        },
      });
      const fullContext = {
        ...ctx,
        user: users[0],
      };
      await controller.patchById(fullContext);

      const cake = await getRepository(Cake).findOne(cakes[1].id);

      if (!cake) {
        throw new Error();
      }

      notStrictEqual(cake.name, cakes[0]);
    });

    it('should return an HttpResponseNotFound if the object does not exist.', async () => {
      const ctx = new Context({
        body: {
          name: 'Black Forest',
        },
        params: {
          id: users[0].id,
        },
      });

      const fullContext = {
        ...ctx,
        user: users[0],
      };

      const response = await controller.patchById(fullContext);

      if (!isHttpResponseNotFound(response)) {
        throw new Error(
          'The returned value should be an HttpResponseNotFound object.',
        );
      }
    });
  });

  describe('has a "put" method that', () => {
    it('should handle requests at PUT /.', () => {
      strictEqual(getHttpMethod(CakeController, 'put'), 'PUT');
      strictEqual(getPath(CakeController, 'put'), '/');
    });

    it('should return a HttpResponseMethodNotAllowed.', () => {
      ok(isHttpResponseMethodNotAllowed(controller.put()));
    });
  });

  describe('has a "putById" method that', () => {
    it('should handle requests at PUT /:id.', () => {
      strictEqual(getHttpMethod(CakeController, 'putById'), 'PUT');
      strictEqual(getPath(CakeController, 'putById'), '/:id');
    });

    it('should update the cake in the database and return it through an HttpResponseOK object.', async () => {
      const ctx = new Context({
        body: {
          name: 'Black Forest',
        },
        params: {
          id: cakes[0].id,
        },
      });

      const fullContext = {
        ...ctx,
        user: users[0],
      };

      const response = await controller.putById(fullContext);

      if (!isHttpResponseOK(response)) {
        throw new Error(
          'The returned value should be an HttpResponseOK object.',
        );
      }

      const cake = await getRepository(Cake).findOne(cakes[0].id);

      if (!cake) {
        throw new Error();
      }

      strictEqual(cake.name, 'Black Forest');

      strictEqual(response.body.data.id, cake.id);
      strictEqual(response.body.data.name, cake.name);
    });

    it('should not update the other cakes.', async () => {
      const ctx = new Context({
        body: {
          text: 'Black Forest',
        },
        params: {
          id: cakes[0].id,
        },
      });

      const fullContext = {
        ...ctx,
        user: users[0],
      };

      await controller.putById(fullContext);

      const cake = await getRepository(Cake).findOne(cakes[1].id);

      if (!cake) {
        throw new Error();
      }

      notStrictEqual(cake.name, 'Black Forest');
    });

    it('should return an HttpResponseNotFound if the object does not exist.', async () => {
      const ctx = new Context({
        body: {
          text: '',
        },
        params: {
          id: users[0].id,
        },
      });

      const fullContext = {
        ...ctx,
        user: users[0],
      };

      const response = await controller.putById(fullContext);

      if (!isHttpResponseNotFound(response)) {
        throw new Error(
          'The returned value should be an HttpResponseNotFound object.',
        );
      }
    });
  });

  describe('has a "delete" method that', () => {
    it('should handle requests at DELETE /.', () => {
      strictEqual(getHttpMethod(CakeController, 'delete'), 'DELETE');
      strictEqual(getPath(CakeController, 'delete'), '/');
    });

    it('should return a HttpResponseMethodNotAllowed.', () => {
      ok(isHttpResponseMethodNotAllowed(controller.delete()));
    });
  });

  describe('has a "deleteById" method that', () => {
    it('should handle requests at DELETE /:id.', () => {
      strictEqual(getHttpMethod(CakeController, 'deleteById'), 'DELETE');
      strictEqual(getPath(CakeController, 'deleteById'), '/:id');
    });

    it('should delete the cake and return an HttpResponseNoContent object.', async () => {
      const ctx = new Context({
        params: {
          id: cakes[0].id,
        },
      });

      const fullContext = {
        ...ctx,
        user: users[0],
      };

      await controller.deleteById(fullContext);

      const cake = await getRepository(Cake).findOne(cakes[0].id);

      strictEqual(cake, undefined);
    });

    it('should not delete the other cakes.', async () => {
      const ctx = new Context({
        params: {
          id: cakes[0].id,
        },
      });

      const fullContext = {
        ...ctx,
        user: users[0],
      };

      await controller.deleteById(fullContext);

      const cake = await getRepository(Cake).findOne(cakes[1].id);

      notStrictEqual(cake, undefined);
    });

    it('should return an HttpResponseNotFound if the cake was not fond.', async () => {
      const ctx = new Context({
        params: {
          id: users[0].id,
        },
      });

      const fullContext = {
        ...ctx,
        user: users[0],
      };

      const response = await controller.deleteById(fullContext);

      if (!isHttpResponseNotFound(response)) {
        throw new Error(
          'The returned value should be an HttpResponseNotFound object.',
        );
      }
    });
  });
});
