import {
  Context,
  Delete,
  Get,
  HttpResponseCreated,
  HttpResponseInternalServerError,
  HttpResponseMethodNotAllowed,
  HttpResponseNotFound,
  HttpResponseOK,
  Patch,
  Post,
  Put,
  ValidateBody,
  ValidateParams,
  ValidateQuery,
} from '@foal/core';
import { JWTRequired } from '@foal/jwt';
import { fetchUser } from '@foal/typeorm';
import { classToPlain, plainToClass } from 'class-transformer';
import * as R from 'ramda';
import { getRepository, In } from 'typeorm';

import {
  Cake,
  CakeDesign,
  CakeToIngredients,
  Ingredients,
  Topings,
  User,
} from '../../entities';
import { CakeImage } from '../../entities/cake/cake-image.entity';
import { userRepository } from '../../entities/user/user.entity';
import { PermissionRequired } from '../../hooks';
import { isTokenBlacklisted } from '../../services/utilities/jwt';
import { Permissions } from '../permissions';
import { getQueryRunner } from '../utils';
import { ICake } from './interfaces';
import { cakeSchema } from './validation-schemas';

export class CakeController {
  @Get('/')
  @ValidateQuery({
    properties: {
      skip: { type: 'number' },
      take: { type: 'number' },
    },
    type: 'object',
  })
  async get(ctx: Context) {
    const cakes = await getRepository(Cake).find({
      skip: ctx.request.query.skip,
      take: ctx.request.query.take,
    });
    return new HttpResponseOK({
      message: 'cakes retrieved seccessfully',
      data: cakes.map(cake => classToPlain(cake)),
    });
  }

  @Get('merchant/:id')
  @ValidateParams({ properties: { id: { type: 'string' } }, type: 'object' })
  async getMerchantCakes(ctx: Context) {
    const id = ctx.request.params.id;
    const user = await userRepository().findOne(id);

    if (!user) {
      return new HttpResponseNotFound({
        message: `user with id ${id} not found`,
      });
    }

    const cakes = await user.cakes;

    return new HttpResponseOK({
      message: 'cakes retrieved successfully',
      data: cakes,
    });
  }

  @Get('/:id')
  @ValidateParams({ properties: { id: { type: 'string' } }, type: 'object' })
  async getById(ctx: Context) {
    const cake = await getRepository(Cake).findOne(ctx.request.params.id);

    if (!cake) {
      return new HttpResponseNotFound({
        message: 'cakes no found',
      });
    }

    return new HttpResponseOK({
      message: 'cakes retrieved successfully',
      data: cake,
    });
  }

  @Post('/')
  @JWTRequired({
    user: fetchUser(User),
    blackList: isTokenBlacklisted,
  })
  @PermissionRequired(Permissions.CreateCakes)
  @ValidateBody(cakeSchema)
  async post(ctx: Context) {
    const { ingredients, name, varieties: designs } = ctx.request.body as ICake;

    const cakeEntity = plainToClass(Cake, {
      name,
      sellerId: ctx.user.id,
    });

    cakeEntity.cakeDesigns = designs.map(
      ({ color, design, price, images, topings }) => {
        const cakeDesign = plainToClass(CakeDesign, { color, design, price });
        cakeDesign.designer = ctx.user;

        if (images && images.length) {
          const cakeImages = images.map(url =>
            plainToClass(CakeImage, { url }),
          );
          cakeDesign.images = cakeImages;
        }

        cakeDesign.topings =
          topings && topings.length
            ? topings.map(toping => plainToClass(Topings, toping))
            : [];
        return cakeDesign;
      },
    );

    const ingredientNames = ingredients.map(({ name }) => name);

    const savedIngredients = await getRepository(Ingredients).find({
      where: {
        name: In(ingredientNames),
      },
      order: {
        name: 'DESC',
      },
    });
    const savedIngredientNames = savedIngredients.map(({ name }) => name);

    const newIngredients = !savedIngredients.length
      ? ingredients
      : ingredients.filter(({ name }) => !savedIngredientNames.includes(name));

    const ingredientEntities = newIngredients.map(({ name }) =>
      plainToClass(Ingredients, { name }),
    );

    const queryRunner = await getQueryRunner();

    try {
      const [cake, cakeIngredients] = await Promise.all([
        queryRunner.manager.save(cakeEntity),
        queryRunner.manager.save(ingredientEntities),
      ]);

      const allIngredients = R.indexBy(R.prop('name'), [
        ...cakeIngredients,
        ...savedIngredients,
      ]);

      const cakeIngredientsEntities = ingredients.map(({ name, quantity }) => {
        return plainToClass(CakeToIngredients, {
          quantity,
          cakeId: cake.id,
          ingredientId: allIngredients[name].id,
        });
      });

      await queryRunner.manager.save(cakeIngredientsEntities);
      await queryRunner.commitTransaction();

      return new HttpResponseCreated({
        message: 'cake created successfully',
        data: classToPlain(cake),
      });
    } catch (error) {
      await queryRunner.rollbackTransaction();

      return new HttpResponseInternalServerError({
        message: 'cake creation failed',
        error: error.message,
      });
    } finally {
      await queryRunner.release();
    }
  }

  @Post('/:id')
  postById() {
    return new HttpResponseMethodNotAllowed();
  }

  @Patch('/')
  patch() {
    return new HttpResponseMethodNotAllowed();
  }

  @Patch('/:id')
  @JWTRequired({
    user: fetchUser(User),
    blackList: isTokenBlacklisted,
  })
  @ValidateParams({ properties: { id: { type: 'string' } }, type: 'object' })
  @ValidateBody({ ...cakeSchema, required: [] })
  @PermissionRequired(Permissions.UpdateCakes)
  async patchById(ctx: Context) {
    const cake = await getRepository(Cake).findOne(ctx.request.params.id);

    if (!cake) {
      return new HttpResponseNotFound({
        message: 'cake not found',
      });
    }

    Object.assign(cake, ctx.request.body);

    await getRepository(Cake).save(cake);

    return new HttpResponseOK({
      message: 'cakes updated successfully',
      data: classToPlain(cake),
    });
  }

  @Put('/')
  put() {
    return new HttpResponseMethodNotAllowed();
  }

  @Put('/:id')
  @JWTRequired({
    user: fetchUser(User),
    blackList: isTokenBlacklisted,
  })
  @ValidateParams({ properties: { id: { type: 'string' } }, type: 'object' })
  @ValidateBody(cakeSchema)
  @PermissionRequired(Permissions.UpdateCakes)
  async putById(ctx: Context) {
    const cake = await getRepository(Cake).findOne(ctx.request.params.id);

    if (!cake) {
      return new HttpResponseNotFound({
        message: 'cake not found',
      });
    }

    Object.assign(cake, ctx.request.body);

    await getRepository(Cake).save(cake);

    return new HttpResponseOK({
      message: 'cakes updated successfully',
      data: classToPlain(cake),
    });
  }

  @Delete('/')
  delete() {
    return new HttpResponseMethodNotAllowed();
  }

  @Delete('/:id')
  @JWTRequired({
    user: fetchUser(User),
    blackList: isTokenBlacklisted,
  })
  @ValidateParams({ properties: { id: { type: 'string' } }, type: 'object' })
  @PermissionRequired(Permissions.DeleteCakes)
  async deleteById(ctx: Context) {
    const cake = await getRepository(Cake).findOne(ctx.request.params.id);

    if (!cake) {
      return new HttpResponseNotFound({
        message: 'cake not found',
      });
    }

    await getRepository(Cake).delete(ctx.request.params.id);

    return new HttpResponseOK({
      message: 'cake deleted successfully',
    });
  }
}
