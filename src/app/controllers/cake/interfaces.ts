import { UnitAmount } from '../../entities/cake/unit-amount';
export interface ICake {
  name: string;
  ingredients: IIngredient[];
  varieties: ICakeVariety[];
}

export interface IIngredient {
  name: string;
  quantity: string;
}

export interface ICakeVariety {
  color: string;
  design: string;
  price: UnitAmount;
  topings: ITopingsEntity[];
  images: ICakeImage[];
}

export interface ICakeImage {
  url: string;
}

export interface ITopingsEntity {
  name: string;
  amount: number;
}
