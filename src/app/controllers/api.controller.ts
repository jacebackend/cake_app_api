import {
  ApiInfo,
  ApiServer,
  controller,
  Get,
  HttpResponseOK,
} from '@foal/core';

import { CakeController } from './cake';
import { OrderController } from './order';
import { UserController } from './user';
@ApiInfo({
  title: 'Cake app',
  version: '1.0.0',
})
@ApiServer({ url: 'http://localhost:5000/api/v1' })
@ApiServer({
  url: 'https://jacebackend-cake-app-api-staging.35.224.43.62.nip.io/api/v1',
})
export class ApiController {
  subControllers = [
    controller('/auth', UserController),
    controller('/cakes', CakeController),
    controller('/orders', OrderController),
  ];

  @Get('/')
  index(ctx) {
    return new HttpResponseOK('Hello world!');
  }
}
