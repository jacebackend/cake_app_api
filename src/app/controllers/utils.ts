import {
  ValidationArguments,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { getConnection, QueryRunner } from 'typeorm';
import * as validate from 'validate-typescript';

import { dependency, ServiceManager } from '@foal/core';
import { classToPlain } from 'class-transformer';
import {
  OwnershipType,
  UserDetails,
  UserType,
} from '../entities/user/interfaces';
import { LoggerService } from '../services/utilities/logger';

export const headerSchema = {
  properties: {
    // tslint:disable-next-line:object-literal-key-quotes
    authorization: { type: 'string' },
  },
  required: ['authorization'],
  type: 'object',
};

export const matchById = <I, T>(
  ids: I[],
  items: T[],
  getId: (item: T) => I,
): (T | undefined)[] => {
  return ids.map(id => items.find(item => getId(item) === id));
};

export const getQueryRunner = async () => {
  const connection = getConnection();
  const queryRunner = connection.createQueryRunner();

  await queryRunner.connect();
  await queryRunner.startTransaction();

  return queryRunner;
};

export const insertQuery = async <T>(
  queryRunner: QueryRunner,
  entity: string,
  values: any[],
): Promise<T[]> => {
  return (await queryRunner.manager
    .createQueryBuilder()
    .insert()
    .into(entity)
    .values(values)
    .returning('*')
    .execute()).generatedMaps as T[];
};

export const summationReducer = (total, current) => total + current;

export const reduceArray = <T>(array: T[], reducer, defaultValue): T =>
  array.length ? array.reduce(reducer) : defaultValue;

@ValidatorConstraint({ name: 'userTypeValidator', async: false })
export class UserTypeValidator implements ValidatorConstraintInterface {
  @dependency
  services: ServiceManager;
  logger: LoggerService;

  adminSchema = {};
  basicUserSchema = {
    phonenumber: validate.RegEx(
      /^(?:254|\+254|0)?(7(?:(?:[129][0-9])|(?:0[0-8])|(4[0-1]))[0-9]{6})$/,
    ),
  };
  merchantSchema = {
    businessName: validate.Type(String),
    primaryAddressOfOperation: validate.Type(String),
    ownership: validate.Options([
      OwnershipType.Partnership,
      OwnershipType.Private,
    ]),
    owners: [
      {
        firstName: validate.Type(String),
        secondName: validate.Type(String),
        sirName: validate.Type(String),
        nationalId: validate.Type(String),
        phonenumber: validate.Type(String),
      },
    ],
  };

  validate(value: UserDetails, args: ValidationArguments) {
    switch (value.kind) {
      case UserType.BasicUser:
        try {
          validate.validate(this.basicUserSchema, value);
          return true;
        } catch (error) {
          console.log(error);
          return false;
        }
      case UserType.Merchant:
        try {
          validate.validate(this.merchantSchema, value);
          return true;
        } catch (error) {
          console.log(error);
          return false;
        }
      default:
        return false;
    }
  }
}

export const successResponse = (data: any) => ({
  message: 'request successful',
  data: classToPlain(data),
});
