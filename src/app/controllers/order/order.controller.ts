import * as R from 'ramda';
import { getRepository } from 'typeorm';

import {
  Context,
  controller,
  Delete,
  Get,
  HttpResponseCreated,
  HttpResponseInternalServerError,
  HttpResponseMethodNotAllowed,
  HttpResponseNoContent,
  HttpResponseNotFound,
  HttpResponseOK,
  Patch,
  Post,
  Put,
  ValidateBody,
  ValidateParams,
  ValidateQuery,
} from '@foal/core';
import { JWTRequired } from '@foal/jwt';
import { fetchUser } from '@foal/typeorm';

import { Order, User } from '../../entities';
import { cakeVarietyRepository } from '../../entities/cake/cake-design.entity';
import {
  OrderCake,
  orderCakeRepository,
} from '../../entities/order/order-cake.entity';
import { orderRepository } from '../../entities/order/order.entity';
import { PermissionRequired } from '../../hooks';
import { isTokenBlacklisted } from '../../services/utilities/jwt';
import { DeliveryController } from '../delivery';
import { Permissions } from '../permissions';
import {
  getQueryRunner,
  insertQuery,
  reduceArray,
  successResponse,
  summationReducer,
} from '../utils';
import { IOrderRequest } from './interfaces';

const orderSchema = {
  additionalProperties: false,
  properties: {
    cakes: {
      properties: {
        id: { type: 'string' },
        quantity: { type: 'number' },
        units: { type: 'number' },
      },
      type: 'array',
    },
  },
  required: ['cakes'],
  type: 'object',
};

export class OrderController {
  subControllers = [controller('/:orderId/deliveries', DeliveryController)];

  @Get('/')
  @ValidateQuery({
    properties: {
      skip: { type: 'number' },
      take: { type: 'number' },
    },
    type: 'object',
  })
  async get(ctx: Context) {
    const orders = await getRepository(Order).find({
      skip: ctx.request.query.skip,
      take: ctx.request.query.take,
      order: {
        createdAt: 'ASC',
      },
    });
    return new HttpResponseOK(
      successResponse(
        orders.length
          ? await Promise.all(orders.map(convertOrderToReponse))
          : [],
      ),
    );
  }

  @Get('/:id')
  @JWTRequired({
    user: fetchUser(User),
    blackList: isTokenBlacklisted,
  })
  @PermissionRequired(Permissions.GetOrder)
  @ValidateParams({ properties: { id: { type: 'string' } }, type: 'object' })
  async getById(ctx: Context) {
    const order = await getRepository(Order).findOne(ctx.request.params.id);

    if (!order) {
      return new HttpResponseNotFound({
        message: 'order not found',
      });
    }

    return new HttpResponseOK(
      successResponse(await convertOrderToReponse(order)),
    );
  }

  @Post('/')
  @JWTRequired({
    user: fetchUser(User),
    blackList: isTokenBlacklisted,
  })
  @PermissionRequired(Permissions.CreateOrders)
  @ValidateBody(orderSchema)
  async post(ctx: Context) {
    const { cakes }: IOrderRequest = ctx.request.body;
    const ids = cakes.map(({ id }) => id);

    const cakeVarieties = await cakeVarietyRepository().findByIds(ids);

    if (cakeVarieties.length !== ids.length) {
      return new HttpResponseNotFound({
        message: 'cakes provided are not found',
        cakes: R.difference(ids, cakeVarieties.map(({ id }) => id)),
      });
    }

    const queryRunner = await getQueryRunner();
    try {
      const order = new Order();
      order.user = ctx.user;

      const savedOrder = await orderRepository().save(order);

      await insertQuery<OrderCake>(
        queryRunner,
        'order_cake',
        cakeVarieties.map(({ id }, index) => {
          const orderUnit = cakes.find(orderunit => orderunit.id === id);
          return {
            cakeVarietyId: id,
            orderId: savedOrder.id,
            orderQuantity: orderUnit
              ? {
                  units: orderUnit.weight,
                  quantity: orderUnit.quantity,
                }
              : {
                  units: 0,
                  quantity: 0,
                },
          };
        }),
      );

      await queryRunner.commitTransaction();

      return new HttpResponseCreated(
        successResponse(await convertOrderToReponse(savedOrder)),
      );
    } catch (error) {
      await queryRunner.rollbackTransaction();

      return new HttpResponseInternalServerError({
        message: 'order creation failed',
        error: error.message,
      });
    } finally {
      await queryRunner.release();
    }
  }

  @Post('/:id')
  postById() {
    return new HttpResponseMethodNotAllowed();
  }

  @Patch('/')
  patch() {
    return new HttpResponseMethodNotAllowed();
  }

  @Patch('/:id')
  async patchById(ctx: Context) {
    return new HttpResponseMethodNotAllowed();
  }

  @Put('/')
  put() {
    return new HttpResponseMethodNotAllowed();
  }

  @Put('/:action/:id')
  @JWTRequired({
    user: fetchUser(User),
    blackList: isTokenBlacklisted,
  })
  @PermissionRequired(Permissions.UpdateOrders)
  @ValidateParams({
    properties: {
      action: { type: 'string' },
      id: { type: 'string' },
    },
    type: 'object',
  })
  @ValidateBody({
    properties: {
      cakes: {
        properties: {
          id: { type: 'string' },
          quantity: { type: 'number' },
          units: { type: 'number' },
        },
        type: 'array',
      },
      status: { type: 'string' },
    },
    type: 'object',
  })
  async putById(ctx: Context) {
    const { action, id } = ctx.request.params;

    const order = await getRepository(Order).findOne(id);

    if (!order) {
      return new HttpResponseNotFound({
        message: 'order not found',
      });
    }

    const cakeVarietyIds: string[] =
      ctx.request.body.cakes && ctx.request.body.cakes.map(({ id }) => id);

    const quantities =
      ctx.request.body.cakes &&
      ctx.request.body.cakes.map(({ weight, quantity }) => ({
        weight,
        quantity,
      }));

    const queryRunner = await getQueryRunner();
    try {
      switch (action) {
        case 'add':
          const newCakes = await cakeVarietyRepository().findByIds(
            cakeVarietyIds,
          );

          if (newCakes.length !== cakeVarietyIds.length) {
            return new HttpResponseNotFound({
              message: 'cakes provided are not found',
              cakes: R.difference(cakeVarietyIds, newCakes.map(({ id }) => id)),
            });
          }

          await queryRunner.manager
            .createQueryBuilder()
            .insert()
            .into(OrderCake)
            .values(
              newCakes.map(({ id }, index) => ({
                cakeVarietyId: id,
                orderId: order.id,
                orderQuantity: {
                  units: quantities[index].weight,
                  quantity: quantities[index].quantity,
                },
              })),
            )
            .onConflict(`("cakeVarietyId", "orderId") DO NOTHING`)
            .returning('*')
            .execute();
          break;
        case 'remove':
          await Promise.all(
            cakeVarietyIds.map(
              async id =>
                await orderCakeRepository().delete({
                  cakeVarietyId: id,
                  orderId: order.id,
                }),
            ),
          );
          break;
        case 'status':
          order.status = ctx.request.body.status;
          await queryRunner.manager.getRepository(Order).save(order);
          break;
        default:
          return new HttpResponseMethodNotAllowed({
            message: 'action not allowed',
          });
      }

      const savedOrder = await queryRunner.manager
        .getRepository(Order)
        .findOne(order.id);

      await queryRunner.commitTransaction();

      return new HttpResponseOK(
        successResponse(await convertOrderToReponse(savedOrder!)),
      );
    } catch (error) {
      await queryRunner.rollbackTransaction();

      return new HttpResponseInternalServerError({
        message: error.message,
      });
    } finally {
      await queryRunner.release();
    }
  }

  @Delete('/')
  delete() {
    return new HttpResponseMethodNotAllowed();
  }

  @Delete('/:id')
  @JWTRequired({
    user: fetchUser(User),
    blackList: isTokenBlacklisted,
  })
  @PermissionRequired(Permissions.DeleteOrders)
  @ValidateParams({ properties: { id: { type: 'string' } }, type: 'object' })
  async deleteById(ctx: Context) {
    const order = await getRepository(Order).findOne(ctx.request.params.id);

    if (!order) {
      return new HttpResponseNotFound();
    }

    await getRepository(Order).delete(ctx.request.params.id);

    return new HttpResponseNoContent();
  }
}

const convertOrderToReponse = async (order: Order) => {
  const fullOrder =
    order.cakes && order.cakes.length
      ? order
      : (await orderRepository().findOne({ where: { id: order.id } }))!;

  const orderCakes = fullOrder.cakes;

  const pricesOfCakes = orderCakes.map(orderCake => {
    const cakeVariety = orderCake.cakeDesign;

    const { quantity, units } = orderCake.orderQuantity;
    const { amount: price } = cakeVariety.price;

    const topingsPrice = reduceArray(
      cakeVariety.topings.map(({ price }) => price),
      summationReducer,
      0,
    );

    const totalPricePerCake = price * units + topingsPrice;

    return totalPricePerCake * quantity;
  });

  const totalPrice = reduceArray(pricesOfCakes, summationReducer, 0);

  return {
    ...fullOrder,
    total: totalPrice,
  };
};
