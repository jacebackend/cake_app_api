// std
import { notStrictEqual, ok, strictEqual } from 'assert';

// 3p
import {
  Context,
  createController,
  getHttpMethod,
  getPath,
  isHttpResponseCreated,
  isHttpResponseMethodNotAllowed,
  isHttpResponseNoContent,
  isHttpResponseNotFound,
  isHttpResponseOK,
} from '@foal/core';
import { Connection, getRepository } from 'typeorm';

// App
import { plainToClass } from 'class-transformer';
import { createDatabaseConnection } from '../../../utils/db';
import {
  Cake,
  CakeDesign,
  CakeToIngredients,
  Ingredients,
  Order,
  User,
} from '../../entities';
import {
  OrderCake,
  orderCakeRepository,
} from '../../entities/order/order-cake.entity';
import { OrderStatus } from '../../entities/order/order.entity';
import {
  mockCake,
  mockCakeVariety,
  mockOrderCake,
  mockUser,
} from '../../tests';
import { OrderController } from './order.controller';

describe('OrderController', () => {
  let controller: OrderController;

  let orders: Order[];
  let cakes: Cake[];
  let users: User[];
  let cakeDesigns: CakeDesign[];

  let connection: Connection;
  let token: string;

  before(async () => {
    connection = await createDatabaseConnection({ synchronize: true });
  });

  after(async () => {
    await connection.synchronize(true);
    await connection.close();
  });

  afterEach(async () => await connection.synchronize(true));

  beforeEach(async () => {
    controller = createController(OrderController);

    const ordersRepository = getRepository(Order);

    const cakeRepository = getRepository(Cake);
    const userRepository = getRepository(User);
    const ingredientsRepository = getRepository(Ingredients);
    const cakeIngredient = getRepository(CakeToIngredients);

    const [ingredients, savedUsers] = await Promise.all([
      ingredientsRepository.save(
        ['sample1', 'sample2', 'sample3'].map(name =>
          plainToClass(Ingredients, { name }),
        ),
      ),
      userRepository.save(
        [
          mockUser({}),
          mockUser({ username: 'test2', email: 'test2@email.com' }),
        ].map(user => plainToClass(User, user)),
      ),
    ]);

    await userRepository.save(savedUsers);

    cakeDesigns = [mockCakeVariety({}), mockCakeVariety({ color: 'red' })].map(
      cakeVariety => plainToClass(CakeDesign, cakeVariety),
    );
    users = savedUsers;

    cakes = await cakeRepository.save([
      plainToClass(Cake, {
        cakeDesigns,
        ...mockCake({ name: 'Test cake', sellerId: users[0].id }),
      }),
      plainToClass(Cake, {
        cakeDesigns,
        ...mockCake({ name: 'Test cake2', sellerId: users[1].id }),
      }),
      plainToClass(Cake, {
        cakeDesigns,
        ...mockCake({ name: 'Test cake3', sellerId: users[1].id }),
      }),
    ]);

    cakeDesigns = await getRepository(CakeDesign).find();

    await cakeIngredient.save([
      plainToClass(CakeToIngredients, {
        cakeId: cakes[0].id,
        ingredientId: ingredients[0].id,
        quantity: '5',
      }),
      plainToClass(CakeToIngredients, {
        cakeId: cakes[0].id,
        ingredientId: ingredients[1].id,
        quantity: '5',
      }),
      plainToClass(CakeToIngredients, {
        cakeId: cakes[0].id,
        ingredientId: ingredients[2].id,
        quantity: '5',
      }),
      plainToClass(CakeToIngredients, {
        cakeId: cakes[1].id,
        ingredientId: ingredients[0].id,
        quantity: '5',
      }),
      plainToClass(CakeToIngredients, {
        cakeId: cakes[1].id,
        ingredientId: ingredients[1].id,
        quantity: '5',
      }),
      plainToClass(CakeToIngredients, {
        cakeId: cakes[1].id,
        ingredientId: ingredients[2].id,
        quantity: '5',
      }),
    ]);

    orders = await ordersRepository.save([
      plainToClass(Order, {
        user: users[0],
        status: OrderStatus.Checkout,
      }),
      plainToClass(Order, {
        user: users[1],
        status: OrderStatus.Checkout,
      }),
    ]);

    await orderCakeRepository().save([
      plainToClass(
        OrderCake,
        mockOrderCake({
          cakeVarietyId: cakeDesigns[0].id,
          orderId: orders[0].id,
        }),
      ),
      plainToClass(
        OrderCake,
        mockOrderCake({
          cakeVarietyId: cakeDesigns[1].id,
          orderId: orders[1].id,
        }),
      ),
    ]);

    token = await users[0].generateToken();
  });

  describe('has a "get" method that', () => {
    it('should handle requests at GET /.', () => {
      strictEqual(getHttpMethod(OrderController, 'get'), 'GET');
      strictEqual(getPath(OrderController, 'get'), '/');
    });

    it('should return an HttpResponseOK object with the order list.', async () => {
      const ctx = new Context({ query: {} });
      const response = await controller.get(ctx);

      if (!isHttpResponseOK(response)) {
        throw new Error(
          'The returned value should be an HttpResponseOK object.',
        );
      }

      if (!Array.isArray(response.body.data)) {
        throw new Error('The response body should be an array of orders.');
      }

      strictEqual(response.body.data.length, 2);
      ok(response.body.data.find(order => order.id === orders[0].id));
      ok(response.body.data.find(order => order.id === orders[1].id));
    });

    it('should support pagination', async () => {
      let ctx = new Context({
        query: {
          take: 1,
        },
      });
      let response = await controller.get(ctx);

      strictEqual(response.body.data.length, 1);

      ctx = new Context({
        query: {
          skip: 1,
        },
      });
      response = await controller.get(ctx);

      strictEqual(response.body.data.length, 1);
    });
  });

  describe('has a "getById" method that', () => {
    it('should handle requests at GET /:id.', () => {
      strictEqual(getHttpMethod(OrderController, 'getById'), 'GET');
      strictEqual(getPath(OrderController, 'getById'), '/:id');
    });

    it('should return an HttpResponseOK object if the order was found.', async () => {
      const ctx = new Context({
        params: {
          id: orders[1].id,
        },
      });
      const response = await controller.getById(ctx);

      if (!isHttpResponseOK(response)) {
        throw new Error(
          'The returned value should be an HttpResponseOK object.',
        );
      }

      strictEqual(response.body.data.id, orders[1].id);
      strictEqual(response.body.data.status, orders[1].status);
    });

    it('should return an HttpResponseNotFound object if the order was not found.', async () => {
      const ctx = new Context({
        params: {
          id: users[0],
        },
      });
      const response = await controller.getById(ctx);

      if (!isHttpResponseNotFound(response)) {
        throw new Error(
          'The returned value should be an HttpResponseNotFound object.',
        );
      }
    });
  });

  describe('has a "post" method that', () => {
    it('should handle requests at POST /.', () => {
      strictEqual(getHttpMethod(OrderController, 'post'), 'POST');
      strictEqual(getPath(OrderController, 'post'), '/');
    });

    it(
      'should create the order in the database and return it through ' +
        'an HttpResponseCreated object.',
      async () => {
        const ctx = new Context({
          headers: {
            authorization: `Bearer ${token}`,
          },
          body: {
            cakes: [
              {
                id: cakeDesigns[1].id,
                weight: 3,
                quantity: 1,
              },
            ],
          },
        });
        const response = await controller.post(ctx);

        if (!isHttpResponseCreated(response)) {
          throw new Error(
            'The returned value should be an HttpResponseCreated object.',
          );
        }

        const order = await getRepository(Order).findOne(response.body.data.id);

        if (!order) {
          throw new Error('No order 3 was found in the database.');
        }

        strictEqual(response.body.data.id, order.id);
      },
    );
  });

  describe('has a "postById" method that', () => {
    it('should handle requests at POST /:id.', () => {
      strictEqual(getHttpMethod(OrderController, 'postById'), 'POST');
      strictEqual(getPath(OrderController, 'postById'), '/:id');
    });

    it('should return a HttpResponseMethodNotAllowed.', () => {
      ok(isHttpResponseMethodNotAllowed(controller.postById()));
    });
  });

  describe('has a "patch" method that', () => {
    it('should handle requests at PATCH /.', () => {
      strictEqual(getHttpMethod(OrderController, 'patch'), 'PATCH');
      strictEqual(getPath(OrderController, 'patch'), '/');
    });

    it('should return a HttpResponseMethodNotAllowed.', () => {
      ok(isHttpResponseMethodNotAllowed(controller.patch()));
    });
  });

  describe('has a "put" method that', () => {
    it('should handle requests at PUT /.', () => {
      strictEqual(getHttpMethod(OrderController, 'put'), 'PUT');
      strictEqual(getPath(OrderController, 'put'), '/');
    });

    it('should return a HttpResponseMethodNotAllowed.', () => {
      ok(isHttpResponseMethodNotAllowed(controller.put()));
    });
  });

  describe('has a "putById" method that', () => {
    it('should handle requests at PUT /:id.', () => {
      strictEqual(getHttpMethod(OrderController, 'putById'), 'PUT');
      strictEqual(getPath(OrderController, 'putById'), '/:action/:id');
    });

    it('should update the order in the database and return it through an HttpResponseOK object.', async () => {
      const ctx = new Context({
        headers: {
          authorization: `Bearer ${token}`,
        },
        body: {
          cakes: [
            {
              id: cakeDesigns[2].id,
              weight: 3,
              quantity: 1,
            },
          ],
        },
        params: {
          action: 'add',
          id: orders[1].id,
        },
      });
      const response = await controller.putById(ctx);

      if (!isHttpResponseOK(response)) {
        throw new Error(
          'The returned value should be an HttpResponseOK object.',
        );
      }

      const order = await getRepository(Order).findOne(orders[1].id);

      if (!order) {
        throw new Error();
      }

      strictEqual(order.cakes.length, 2);

      strictEqual(response.body.data.id, order.id);
    });

    it('should not update the other orders.', async () => {
      const ctx = new Context({
        headers: {
          authorization: `Bearer ${token}`,
        },
        body: {
          cakes: [cakeDesigns[2]],
        },
        params: {
          action: 'remove',
          id: orders[1].id,
        },
      });
      await controller.putById(ctx);

      const order = await getRepository(Order).findOne(orders[0].id);

      if (!order) {
        throw new Error();
      }

      strictEqual(order.cakes.length, 1);
    });

    it('should return an HttpResponseNotFound if the object does not exist.', async () => {
      const ctx = new Context({
        headers: {
          authorization: `Bearer ${token}`,
        },
        body: {
          text: '',
        },
        params: {
          id: users[0],
        },
      });
      const response = await controller.putById(ctx);

      if (!isHttpResponseNotFound(response)) {
        throw new Error(
          'The returned value should be an HttpResponseNotFound object.',
        );
      }
    });
  });

  describe('has a "delete" method that', () => {
    it('should handle requests at DELETE /.', () => {
      strictEqual(getHttpMethod(OrderController, 'delete'), 'DELETE');
      strictEqual(getPath(OrderController, 'delete'), '/');
    });

    it('should return a HttpResponseMethodNotAllowed.', () => {
      ok(isHttpResponseMethodNotAllowed(controller.delete()));
    });
  });

  describe('has a "deleteById" method that', () => {
    it('should handle requests at DELETE /:id.', () => {
      strictEqual(getHttpMethod(OrderController, 'deleteById'), 'DELETE');
      strictEqual(getPath(OrderController, 'deleteById'), '/:id');
    });

    it('should delete the order and return an HttpResponseNoContent object.', async () => {
      const ctx = new Context({
        headers: {
          authorization: `Bearer ${token}`,
        },
        params: {
          id: orders[0].id,
        },
      });
      const response = await controller.deleteById(ctx);

      if (!isHttpResponseNoContent(response)) {
        throw new Error(
          'The returned value should be an HttpResponseNoContent object.',
        );
      }

      const order = await getRepository(Order).findOne(orders[0].id);

      strictEqual(order, undefined);
    });

    it('should not delete the other orders.', async () => {
      const ctx = new Context({
        headers: {
          authorization: `Bearer ${token}`,
        },
        params: {
          id: orders[0].id,
        },
      });
      const response = await controller.deleteById(ctx);

      if (!isHttpResponseNoContent(response)) {
        throw new Error(
          'The returned value should be an HttpResponseNoContent object.',
        );
      }

      const order = await getRepository(Order).findOne(orders[1].id);

      notStrictEqual(order, undefined);
    });

    it('should return an HttpResponseNotFound if the order was not fond.', async () => {
      const ctx = new Context({
        headers: {
          authorization: `Bearer ${token}`,
        },
        params: {
          id: users[0],
        },
      });
      const response = await controller.deleteById(ctx);

      if (!isHttpResponseNotFound(response)) {
        throw new Error(
          'The returned value should be an HttpResponseNotFound object.',
        );
      }
    });
  });
});
