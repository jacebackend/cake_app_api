export interface IOrderRequest {
  cakes: CakeOrderUnit[];
}

export interface CakeOrderUnit {
  id: string;
  weight: number;
  quantity: number;
}
