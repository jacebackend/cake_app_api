import { CakeUnit, UnitAmount } from './entities/cake/unit-amount';
import { OrderUnit } from './entities/order/order-unit';
export interface MockCakeSpec {
  name: string;
  sellerId: string;
  price?: string;
  ingredients?: MockIngredientsSpec[];
}

export interface MockUserSpec {
  username?: string;
  email?: string;
  password?: string;
}

export interface MockIngredientsSpec {
  name: string;
}

export interface MockCakeVarietySpec {
  color?: string;
  design?: string;
  price?: UnitAmount;
}

export interface MockOrderCakeSpec {
  cakeVarietyId: string;
  orderId: string;
  orderQuantity?: OrderUnit;
}

export const mockCake = (spec: MockCakeSpec) => ({
  name: spec.name,
  sellerId: spec.sellerId,
  price: spec.price || '10',
  ingredients: spec.ingredients,
});

export const mockIngredient = (spec: MockIngredientsSpec) => ({
  name: spec.name,
});

export const mockUser = (spec: MockUserSpec) => ({
  username: spec.username || 'test',
  email: spec.email || 'tes@email.com',
  password: spec.password || 'mynewawsometestpassword',
  userDetails: {
    kind: 'basic-user',
    phonenumber: '0724000000',
  },
});

export const mockCakeVariety = (spec: MockCakeVarietySpec) => ({
  color: spec.color || 'orange',
  design: spec.design || 'Double cake',
  price: spec.price || {
    unit: CakeUnit.Kg,
    amount: 200,
  },
});

export const mockOrderCake = (spec: MockOrderCakeSpec) => ({
  cakeVarietyId: spec.cakeVarietyId,
  orderId: spec.orderId,
  orderQuantity: spec.orderQuantity || {
    units: 1,
    quantity: 1,
  },
});

export const getMockCakeRequestBody = () => ({
  name: 'Red velvet',
  ingredients: [
    {
      name: 'sugar',
      quantity: '12',
    },
    {
      name: 'spice',
      quantity: '11',
    },
    {
      name: 'everthing red',
      quantity: '10',
    },
  ],
  varieties: [
    {
      color: 'orange',
      design: 'Double cake',
      price: {
        unit: CakeUnit.Kg,
        amount: 200,
      },
      topings: [
        {
          name: 'flakes',
          price: 200,
        },
      ],
    },
    {
      color: 'red',
      design: 'Double cake',
      price: {
        unit: CakeUnit.Kg,
        amount: 200,
      },
      topings: [
        {
          name: 'dust flakes',
          price: 30,
        },
      ],
    },
    {
      color: 'orange',
      design: 'Double cake',
      price: {
        unit: CakeUnit.Kg,
        amount: 200,
      },
      topings: [
        {
          name: 'f',
          price: 20,
        },
      ],
    },
  ],
});
