import { Exclude } from 'class-transformer';
import {
  Column,
  Entity,
  getRepository,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryColumn,
} from 'typeorm';

import { CakeDesign } from '../cake/cake-design.entity';
import { OrderUnit } from './order-unit';
import { Order } from './order.entity';

@Entity()
@Index(['cakeVarietyId', 'orderId'], { unique: true })
export class OrderCake {
  @PrimaryColumn()
  cakeVarietyId: string;

  @PrimaryColumn()
  @Exclude({ toPlainOnly: true })
  orderId: string;

  @Column(type => OrderUnit)
  orderQuantity: OrderUnit;

  @ManyToOne(type => CakeDesign, cakeDesign => cakeDesign.orders, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
    eager: true,
  })
  @JoinColumn({ name: 'cakeVarietyId' })
  cakeDesign: CakeDesign;

  @ManyToOne(type => Order, order => order.cakes, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'orderId' })
  order: Promise<Order>;
}

export const orderCakeRepository = () => getRepository(OrderCake);
