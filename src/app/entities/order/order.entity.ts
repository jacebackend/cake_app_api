import { Column, Entity, getRepository, ManyToOne, OneToMany } from 'typeorm';

import { Base } from '../base';
import { User } from '../user';
import { OrderCake } from './order-cake.entity';

export enum OrderStatus {
  Checkout = 'checkout',
  Delivery = 'delivery',
  Fullfilled = 'fullfilled',
}

@Entity()
export class Order extends Base {
  @Column({
    type: 'enum',
    enum: OrderStatus,
    default: OrderStatus.Checkout,
  })
  status: OrderStatus;

  @OneToMany(type => OrderCake, orderCake => orderCake.order, {
    cascade: true,
    eager: true,
  })
  cakes: OrderCake[];

  @ManyToOne(type => User, user => user, {
    eager: true,
  })
  user: User;
}

export const orderRepository = () => getRepository(Order);
