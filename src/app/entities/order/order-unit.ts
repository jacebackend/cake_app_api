import { Column, Entity } from 'typeorm';

@Entity()
export class OrderUnit {
  @Column()
  units: number;

  @Column()
  quantity: number;
}
