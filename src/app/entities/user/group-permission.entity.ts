import { Entity, Index, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';

import { Group } from './group.entity';
import { Permission } from './permission.entity';

@Entity()
@Index(['groupName', 'permissionName'], { unique: true })
export class GroupPermission {
  @PrimaryColumn()
  groupName: string;

  @PrimaryColumn()
  permissionName: string;

  @ManyToOne(type => Group, group => group.groupPermissions, { primary: true })
  @JoinColumn({ name: 'groupName' })
  group: Group;

  @ManyToOne(type => Permission, permission => permission.permissionGroup, {
    primary: true,
  })
  @JoinColumn({ name: 'permissionName' })
  permission: Permission;
}
