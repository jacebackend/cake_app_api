import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class TokenBlacklist {
  @PrimaryGeneratedColumn()
  id: string;

  @Column()
  token: string;

  @CreateDateColumn()
  createdTime: Date;
}
