export enum SignUpType {
  Legacy = 'legacy',
  Social = 'social',
}

export enum UserType {
  BasicUser = 'basic-user',
  Merchant = 'merchant',
  Admin = 'admin',
}

export enum OwnershipType {
  Private = 'private',
  Partnership = 'partnership',
}

export interface Owner {
  firstName: string;
  secondName: string;
  sirName: string;
  nationalId: string;
  phonenumber: string;
}

export interface BaseUserDetails {
  phonenumber: string;
}

export interface BasicUserDetails extends BaseUserDetails {
  kind: UserType.BasicUser;
}

export interface MerchantDetails extends BaseUserDetails {
  kind: UserType.Merchant;
  businessName: string;
  ownership: OwnershipType;
  owners: Owner[];
  primaryAddressOfOperation: string;
  isVerified: boolean;
}

export interface AdminDetails extends BaseUserDetails {
  kind: UserType.Admin;
}

export type UserDetails = BaseUserDetails &
  (BasicUserDetails | MerchantDetails | AdminDetails);
