import {
  Column,
  Entity,
  getRepository,
  Index,
  OneToMany,
  PrimaryColumn,
} from 'typeorm';

import { GroupPermission } from './group-permission.entity';
import { UserGroup } from './user-group.entity';

export enum UserGroups {
  BasicUser = 'basic-user',
  GuestUser = 'guest-user',
  AdminUser = 'admin',
}

@Entity()
export class Group {
  @PrimaryColumn()
  @Index({ unique: true })
  name: string;

  @Column()
  description: string;

  @OneToMany(type => UserGroup, userGroup => userGroup.group)
  groupUsers: Promise<UserGroup[]>;

  @OneToMany(
    type => GroupPermission,
    groupPermission => groupPermission.group,
    {
      cascade: true,
      eager: true,
    },
  )
  groupPermissions: GroupPermission[];
}

export const groupRepository = () => getRepository(Group);
