import { Exclude } from 'class-transformer';
import { IsEmail, Validate } from 'class-validator';
import { sign } from 'jsonwebtoken';
import { Column, Entity, getRepository, Index, OneToMany } from 'typeorm';

import { Config, encryptPassword, verifyPassword } from '@foal/core';

import { UserTypeValidator } from '../../controllers/utils';
import { Base } from '../base';
import { CakeDesign } from '../cake/cake-design.entity';
import { Cake } from '../cake/cake.entity';
import { Order } from '../order/order.entity';
import { SignUpType, UserDetails, UserType } from './interfaces';
import { UserGroup } from './user-group.entity';

@Entity()
export class User extends Base {
  @Column({ unique: true, nullable: true })
  username: string;

  @Column({ unique: true })
  @Index({ unique: true })
  @IsEmail({}, { message: 'email provided is not valid' })
  email: string;

  @Column({ nullable: true })
  @Exclude({ toPlainOnly: true })
  password: string;

  @Column({ default: false })
  @Exclude({ toPlainOnly: true })
  isVerified: boolean;

  @Column({
    type: 'enum',
    enum: SignUpType,
    default: SignUpType.Legacy,
  })
  @Exclude({ toPlainOnly: true })
  signUpType: SignUpType;

  @Column({
    type: 'enum',
    enum: UserType,
    default: UserType.BasicUser,
  })
  userType: UserType;

  @Column({ type: 'jsonb' })
  @Validate(UserTypeValidator)
  userDetails: UserDetails;

  @OneToMany(type => UserGroup, userGroup => userGroup.user, {
    cascade: true,
    eager: true,
  })
  @Exclude({ toPlainOnly: true })
  userGroups: UserGroup[];

  @OneToMany(type => Cake, cake => cake.seller)
  cakes: Promise<Cake[]>;

  @OneToMany(type => Order, order => order.user)
  orders: Promise<Order[]>;

  @OneToMany(type => CakeDesign, cakeDesign => cakeDesign.designer)
  cakeDesigns: Promise<CakeDesign[]>;

  async setPassword(password: string) {
    this.password = await encryptPassword(password);
  }

  async comparePassword(password: string) {
    return await verifyPassword(password, this.password);
  }

  async generateToken() {
    return await sign(
      {
        sub: this.id.toString(),
        email: this.email,
      } as any,
      Config.get<string>('settings.jwt.secretOrPublicKey'),
      { expiresIn: '1h' },
    );
  }

  async hasPerm(perm: string) {
    if (!this.userGroups || !this.userGroups.length) {
      return false;
    }

    const result = this.userGroups.filter(
      ({ group }) =>
        !!group.groupPermissions.find(
          ({ permissionName }) => permissionName === perm,
        ),
    );

    return !!result.length;
  }
}

export const userRepository = () => getRepository(User);
