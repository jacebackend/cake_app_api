import { Column, Entity, Index, OneToMany, PrimaryColumn } from 'typeorm';

import { GroupPermission } from './group-permission.entity';

@Entity()
export class Permission {
  @PrimaryColumn()
  @Index({ unique: true })
  name: string;

  @Column()
  description: string;

  @OneToMany(
    type => GroupPermission,
    groupPermission => groupPermission.permission,
    {
      cascade: true,
      eager: true,
    },
  )
  permissionGroup: GroupPermission[];
}
