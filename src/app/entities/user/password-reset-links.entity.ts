import { Column, Entity, Index } from 'typeorm';

import { Base } from '../base';

@Entity()
export class PasswordResetLinks extends Base {
  @Column()
  @Index()
  email: string;

  @Column()
  link: string;

  @Column({ default: true })
  isValid: boolean;
}
