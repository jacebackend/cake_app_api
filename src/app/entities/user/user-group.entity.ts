import {
  Entity,
  getRepository,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryColumn,
} from 'typeorm';

import { Group } from './group.entity';
import { User } from './user.entity';

@Entity()
@Index(['groupName', 'userId'], { unique: true })
export class UserGroup {
  @PrimaryColumn()
  groupName: string;

  @PrimaryColumn()
  userId: string;

  @ManyToOne(type => Group, group => group.groupUsers, {
    primary: true,
    eager: true,
  })
  @JoinColumn({ name: 'groupName' })
  group: Group;

  @ManyToOne(type => User, user => user.userGroups, {
    primary: true,
  })
  @JoinColumn({ name: 'userId' })
  user: User;
}

export const userGroupRepository = () => getRepository(UserGroup);
