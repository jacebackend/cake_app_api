import { Column, Entity, ManyToMany, PrimaryGeneratedColumn } from 'typeorm';

import { CakeDesign } from './cake-design.entity';

@Entity()
export class Topings {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @Column()
  price: number;

  @ManyToMany(type => CakeDesign, cakeDesign => cakeDesign.topings, {
    nullable: true,
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  cakeDesigns: Promise<CakeDesign>;
}
