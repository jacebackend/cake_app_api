import {
  Column,
  Entity,
  Index,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { CakeToIngredients } from './cake-to-ingredients.entity';

@Entity()
export class Ingredients {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ unique: true })
  @Index({ unique: true })
  name: string;

  @OneToMany(
    type => CakeToIngredients,
    cakeToIngredients => cakeToIngredients.ingredient,
    {
      cascade: true,
    },
  )
  cakeToIngredient: Promise<CakeToIngredients[]>;
}
