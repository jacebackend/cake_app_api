import { Column, Entity, JoinColumn, ManyToOne, OneToMany } from 'typeorm';

import { Base } from '../base';
import { User } from '../user';
import { CakeDesign } from './cake-design.entity';
import { CakeToIngredients } from './cake-to-ingredients.entity';

@Entity()
export class Cake extends Base {
  @Column()
  name: string;

  @Column()
  sellerId: number;

  @ManyToOne(type => User, user => user.cakes)
  @JoinColumn({ name: 'sellerId' })
  seller: Promise<User>;

  @OneToMany(
    type => CakeToIngredients,
    cakeToIngredients => cakeToIngredients.cake,
    {
      cascade: true,
      onDelete: 'CASCADE',
    },
  )
  cakeToIngredient: Promise<CakeToIngredients[]>;

  @OneToMany(type => CakeDesign, cakeDesign => cakeDesign.cake, {
    cascade: true,
    eager: true,
    onDelete: 'CASCADE',
  })
  cakeDesigns: CakeDesign[];
}
