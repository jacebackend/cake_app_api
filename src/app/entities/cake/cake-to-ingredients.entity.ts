import {
  Column,
  CreateDateColumn,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryColumn,
} from 'typeorm';

import { Cake } from './cake.entity';
import { Ingredients } from './ingredients.entity';

@Entity()
@Index(['cakeId', 'ingredientId'], { unique: true })
export class CakeToIngredients {
  @PrimaryColumn()
  cakeId: string;

  @PrimaryColumn()
  ingredientId: string;

  @Column()
  quantity: string;

  @CreateDateColumn()
  createdDate: Date;

  @ManyToOne(type => Cake, cake => cake.cakeToIngredient, {
    primary: true,
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'cakeId' })
  cake!: Promise<Cake>;

  @ManyToOne(type => Ingredients, ingredients => ingredients.cakeToIngredient, {
    primary: true,
  })
  @JoinColumn({ name: 'ingredientId' })
  ingredient!: Promise<Ingredients>;
}
