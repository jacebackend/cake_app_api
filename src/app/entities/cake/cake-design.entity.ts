import {
  Column,
  Entity,
  getRepository,
  Index,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
} from 'typeorm';

import { Base } from '../base';
import { OrderCake } from '../order/order-cake.entity';
import { User } from '../user/user.entity';
import { CakeImage } from './cake-image.entity';
import { Cake } from './cake.entity';
import { Topings } from './topings.entity';
import { UnitAmount } from './unit-amount';

export enum CakeDesignType {
  Default = 'default',
  Custom = 'custom',
}

@Entity()
export class CakeDesign extends Base {
  @Column()
  color: string;

  @Column()
  design: string;

  @Column(type => UnitAmount)
  price: UnitAmount;

  @Column({
    type: 'enum',
    enum: CakeDesignType,
    default: CakeDesignType.Default,
  })
  @Index()
  type: CakeDesignType;

  @ManyToOne(type => User, user => user.cakeDesigns)
  designer: User;

  @ManyToOne(type => Cake, cake => cake.cakeDesigns, {
    onDelete: 'CASCADE',
  })
  cake: Cake;

  @OneToMany(type => OrderCake, order => order.order)
  orders: Promise<OrderCake[]>;

  @OneToMany(type => CakeImage, cakeImage => cakeImage.cake, {
    eager: true,
    cascade: true,
    nullable: true,
  })
  images: CakeImage[];

  @ManyToMany(type => Topings, topings => topings.cakeDesigns, {
    nullable: true,
    cascade: true,
    eager: true,
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinTable()
  topings: Topings[];
}

export const cakeVarietyRepository = () => getRepository(CakeDesign);
