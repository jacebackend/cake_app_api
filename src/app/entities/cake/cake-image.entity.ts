import { Column, Entity, ManyToOne } from 'typeorm';

import { Base } from '../base';
import { CakeDesign } from './cake-design.entity';

@Entity()
export class CakeImage extends Base {
  @Column()
  url: string;

  @ManyToOne(type => CakeDesign, cakeDesign => cakeDesign.images)
  cake: CakeDesign;
}
