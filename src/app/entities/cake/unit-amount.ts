import { Column, Entity } from 'typeorm';

export enum CakeUnit {
  Kg = 'kg',
  Gram = 'g',
  Piece = 'piece',
  Dozen = 'dozen',
}

@Entity()
export class UnitAmount {
  @Column({
    type: 'enum',
    enum: CakeUnit,
    default: CakeUnit.Kg,
  })
  unit: CakeUnit;

  @Column()
  amount: number;
}
