import { Column, Entity, getRepository, JoinColumn, OneToOne } from 'typeorm';

import { utcDateTransformer } from '../../../utils/entities';
import { Base } from '../base';
import { Order } from '../order';
import { DeliveryAddress, DeliveryStatus, DeliveryType } from './interfaces';

@Entity()
export class Delivery extends Base {
  @Column({
    type: 'enum',
    enum: DeliveryStatus,
    default: DeliveryStatus.Preparation,
  })
  status: DeliveryStatus;

  @Column({
    type: 'enum',
    enum: DeliveryType,
    default: DeliveryType.PickUp,
  })
  type: DeliveryType;

  @Column({
    transformer: utcDateTransformer,
  })
  date: Date;

  @Column({
    type: 'jsonb',
    nullable: true,
  })
  address?: DeliveryAddress;

  @OneToOne(type => Order, { eager: true })
  @JoinColumn()
  order: Order;
}

export const deliveryRepository = () => getRepository(Delivery);
