export enum DeliveryStatus {
  Preparation = 'preparation',
  Ready = 'ready',
  Dispatched = 'dispatch',
  Completed = 'completed',
}

export enum DeliveryType {
  PickUp = 'pick-up',
  Deliver = 'deliver',
}

export interface DeliveryAddress {
  address: string;
  latitude: string;
  longitude: string;
}
