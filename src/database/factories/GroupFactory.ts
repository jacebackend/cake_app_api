import * as Faker from 'faker';
import { define } from 'typeorm-seeding';
import { Group } from '../../app/entities';

define(Group, (faker: typeof Faker, settings?: { roleName: string }) => {
  const group = new Group();

  group.name = settings!.roleName;
  group.description = faker.fake(settings!.roleName);

  return group;
});
