import * as Faker from 'faker';
import { define } from 'typeorm-seeding';

import { UserGroup } from '../../app/entities';

define(UserGroup, (
  faker: typeof Faker,
  settings?: { groupId: string; userId: string },
) => {
  const userGroup = new UserGroup();

  userGroup.groupName = settings!.groupId;
  userGroup.userId = settings!.userId;

  return userGroup;
});
