import * as Faker from 'faker';
import { define } from 'typeorm-seeding';

import { GroupPermission } from '../../app/entities';

define(GroupPermission, (
  faker: typeof Faker,
  settings?: { groupId: string; permissionId: string },
) => {
  const groupPermission = new GroupPermission();

  groupPermission.groupName = settings!.groupId;
  groupPermission.permissionName = settings!.permissionId;

  return groupPermission;
});
