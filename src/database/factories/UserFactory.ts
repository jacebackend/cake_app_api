import * as Faker from 'faker';
import { define } from 'typeorm-seeding';

import { User } from '../../app/entities';

define(User, (faker: typeof Faker, settings?: {}) => {
  const gender = faker.random.number(1);
  const username = faker.name.firstName(gender);
  const email = faker.internet.email(username);

  const user = new User();

  user.username = username;
  user.email = email;
  user.isVerified = true;

  return user;
});
