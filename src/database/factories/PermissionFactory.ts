import * as Faker from 'faker';
import { define } from 'typeorm-seeding';
import { Permission } from '../../app/entities';

define(Permission, (faker: typeof Faker, settings?: { roleName: string }) => {
  const permission = new Permission();

  permission.description = faker.fake(settings!.roleName);
  permission.name = settings!.roleName;

  return permission;
});
