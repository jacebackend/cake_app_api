import { Connection } from 'typeorm';
import { Factory, Seeder } from 'typeorm-seeding';

import {
  Group,
  GroupPermission,
  Permission,
  User,
  UserGroup,
} from '../../app/entities';

export default class CreateUsers implements Seeder {
  async run(factory: Factory, connection: Connection): Promise<any> {
    const [cakePermissions, userPermissions] = await Promise.all([
      Promise.all([
        factory(Permission)({ roleName: 'create-cake' }).seed(),
        factory(Permission)({ roleName: 'delete-cake' }).seed(),
        factory(Permission)({ roleName: 'update-cake' }).seed(),
        factory(Permission)({ roleName: 'view-cake' }).seed(),
      ]),
      Promise.all([
        factory(Permission)({ roleName: 'archive-user' }).seed(),
        factory(Permission)({ roleName: 'create-permission' }).seed(),
      ]),
    ]);

    const [adminGroup, basicGroup] = await Promise.all([
      factory(Group)({
        roleName: 'admin',
      }).seed(),
      factory(Group)({
        roleName: 'basic-user',
      }).seed(),
    ]);

    await Promise.all([
      Promise.all(
        cakePermissions.map(({ name }) =>
          factory(GroupPermission)({
            groupId: basicGroup.name,
            permissionId: name,
          }).seed(),
        ),
      ),
      Promise.all(
        [...cakePermissions, ...userPermissions].map(({ name }) =>
          factory(GroupPermission)({
            groupId: adminGroup.name,
            permissionId: name,
          }).seed(),
        ),
      ),
    ]);

    const basicUsers = await factory(User)()
      .map(async (user: User) => {
        await user.setPassword('myawsomecakepassword');
        return user;
      })
      .seedMany(5);
    const adminUser = await factory(User)()
      .map(async (user: User) => {
        await user.setPassword('myawsomecakepassword');
        return user;
      })
      .seed();

    await Promise.all([
      Promise.all(
        basicUsers.map(async ({ id }) => {
          await factory(UserGroup)({
            userId: id,
            groupId: basicGroup.name,
          }).seed();
        }),
      ),
      factory(UserGroup)({
        userId: adminUser.id,
        groupId: adminGroup.name,
      }).seed(),
    ]);
  }
}
