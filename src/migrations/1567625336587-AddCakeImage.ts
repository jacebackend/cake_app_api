import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddCakeImage1567625336587 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TABLE "cake_image" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "url" character varying NOT NULL, "cakeId" uuid, CONSTRAINT "PK_06da74f7f8116001e066380acb5" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "cake_image" ADD CONSTRAINT "FK_0df88a5f57f9ca17a5690f9cbbd" FOREIGN KEY ("cakeId") REFERENCES "cake_design"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "cake_image" DROP CONSTRAINT "FK_0df88a5f57f9ca17a5690f9cbbd"`,
    );
    await queryRunner.query(`DROP TABLE "cake_image"`);
  }
}
