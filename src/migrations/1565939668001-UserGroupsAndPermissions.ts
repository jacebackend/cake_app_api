import { MigrationInterface, QueryRunner } from 'typeorm';

export class UserGroupsAndPermissions1565939668001
  implements MigrationInterface {
  groups = ['admin', 'guest-user', 'basic-user', 'merchant'];
  resources = ['user', 'cakes', 'topings', 'orders', 'deliveries'];

  permissions = this.resources.map(createResourceRoles);

  userPermissions = this.permissions[0];
  cakesPermissions = this.permissions[1];
  topingsPermissions = this.permissions[2];
  ordersPermissions = this.permissions[3];
  deliveriesPermissions = this.permissions[4];

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into('permission')
      .values(
        this.permissions.reduce((total, current) => [...total, ...current]),
      )
      .execute();

    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into('group')
      .values(this.groups.map(createGroup))
      .execute();

    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into('group_permission')
      .values(
        this.permissions
          .map(resourcePerm =>
            resourcePerm.map(permissionName => ({
              permissionName: permissionName.name,
              groupName: 'admin',
            })),
          )
          .reduce((total, current) => [...total, ...current]),
      )
      .execute();

    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into('group_permission')
      .values([
        ...[...this.cakesPermissions]
          .filter(permission =>
            ['get', 'view'].includes(permission.name.split('-')[0]),
          )
          .map(permissionName => ({
            permissionName: permissionName.name,
            groupName: 'basic-user',
          })),
        ...[
          ...this.topingsPermissions,
          ...this.ordersPermissions,
          ...this.deliveriesPermissions,
        ].map(permissionName => ({
          permissionName: permissionName.name,
          groupName: 'basic-user',
        })),
      ])
      .execute();

    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into('group_permission')
      .values(
        [
          ...this.cakesPermissions,
          ...this.topingsPermissions,
          ...this.ordersPermissions,
          ...this.deliveriesPermissions,
        ].map(permissionName => ({
          permissionName: permissionName.name,
          groupName: 'merchant',
        })),
      )
      .execute();

    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into('group_permission')
      .values(
        this.cakesPermissions
          .filter(perm => perm.name.split('-')[0] === 'view')
          .map(permissionName => ({
            permissionName: permissionName.name,
            groupName: 'guest-user',
          })),
      )
      .execute();
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.manager.getRepository('group_permission').clear();

    await queryRunner.manager.getRepository('group').clear();

    await queryRunner.manager.getRepository('permission').clear();
  }
}

const createResourceRoles = (resource: string) =>
  ['create', 'update', 'delete', 'view', 'get'].map(role => ({
    name: `${role}-${resource}`,
    description: `role required to ${role} ${resource}`,
  }));

const createGroup = (name: string) => ({
  name,
  description: `${name} group`,
});
