import { MigrationInterface, QueryRunner } from 'typeorm';

export class Version1Migrations1565939526906 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TABLE "ingredients" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying NOT NULL, CONSTRAINT "UQ_a955029b22ff66ae9fef2e161f8" UNIQUE ("name"), CONSTRAINT "PK_9240185c8a5507251c9f15e0649" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE UNIQUE INDEX "IDX_a955029b22ff66ae9fef2e161f" ON "ingredients" ("name") `,
    );
    await queryRunner.query(
      `CREATE TYPE "order_status_enum" AS ENUM('checkout', 'delivery', 'fullfilled')`,
    );
    await queryRunner.query(
      `CREATE TABLE "order" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "status" "order_status_enum" NOT NULL DEFAULT 'checkout', "userId" uuid, CONSTRAINT "PK_1031171c13130102495201e3e20" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "order_cake" ("cakeVarietyId" uuid NOT NULL, "orderId" uuid NOT NULL, "orderQuantityUnits" integer NOT NULL, "orderQuantityQuantity" integer NOT NULL, CONSTRAINT "PK_e4b26c2272fcb278799fe659e75" PRIMARY KEY ("cakeVarietyId", "orderId"))`,
    );
    await queryRunner.query(
      `CREATE UNIQUE INDEX "IDX_e4b26c2272fcb278799fe659e7" ON "order_cake" ("cakeVarietyId", "orderId") `,
    );
    await queryRunner.query(
      `CREATE TABLE "topings" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying NOT NULL, "price" integer NOT NULL, CONSTRAINT "PK_7b41649c7836af0468095182f23" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TYPE "cake_design_type_enum" AS ENUM('default', 'custom')`,
    );
    await queryRunner.query(
      `CREATE TYPE "cake_design_priceunit_enum" AS ENUM('kg', 'g', 'piece', 'dozen')`,
    );
    await queryRunner.query(
      `CREATE TABLE "cake_design" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "color" character varying NOT NULL, "design" character varying NOT NULL, "type" "cake_design_type_enum" NOT NULL DEFAULT 'default', "designerId" uuid, "cakeId" uuid, "priceUnit" "cake_design_priceunit_enum" NOT NULL DEFAULT 'kg', "priceAmount" integer NOT NULL, CONSTRAINT "PK_7e4303c807211ea60f40b36d119" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_63df9c3583a3f70abf04850d07" ON "cake_design" ("type") `,
    );
    await queryRunner.query(
      `CREATE TABLE "permission" ("name" character varying NOT NULL, "description" character varying NOT NULL, CONSTRAINT "PK_240853a0c3353c25fb12434ad33" PRIMARY KEY ("name"))`,
    );
    await queryRunner.query(
      `CREATE UNIQUE INDEX "IDX_240853a0c3353c25fb12434ad3" ON "permission" ("name") `,
    );
    await queryRunner.query(
      `CREATE TABLE "group_permission" ("groupName" character varying NOT NULL, "permissionName" character varying NOT NULL, CONSTRAINT "PK_84faca2f810840896c78de1d9aa" PRIMARY KEY ("groupName", "permissionName"))`,
    );
    await queryRunner.query(
      `CREATE UNIQUE INDEX "IDX_84faca2f810840896c78de1d9a" ON "group_permission" ("groupName", "permissionName") `,
    );
    await queryRunner.query(
      `CREATE TABLE "group" ("name" character varying NOT NULL, "description" character varying NOT NULL, CONSTRAINT "PK_8a45300fd825918f3b40195fbdc" PRIMARY KEY ("name"))`,
    );
    await queryRunner.query(
      `CREATE UNIQUE INDEX "IDX_8a45300fd825918f3b40195fbd" ON "group" ("name") `,
    );
    await queryRunner.query(
      `CREATE TABLE "user_group" ("groupName" character varying NOT NULL, "userId" uuid NOT NULL, CONSTRAINT "PK_a907a1885bed948d879c278ee3c" PRIMARY KEY ("groupName", "userId"))`,
    );
    await queryRunner.query(
      `CREATE UNIQUE INDEX "IDX_a907a1885bed948d879c278ee3" ON "user_group" ("groupName", "userId") `,
    );
    await queryRunner.query(
      `CREATE TYPE "user_signuptype_enum" AS ENUM('legacy', 'social')`,
    );
    await queryRunner.query(
      `CREATE TYPE "user_usertype_enum" AS ENUM('basic-user', 'merchant', 'admin')`,
    );
    await queryRunner.query(
      `CREATE TABLE "user" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "username" character varying, "email" character varying NOT NULL, "password" character varying, "isVerified" boolean NOT NULL DEFAULT false, "signUpType" "user_signuptype_enum" NOT NULL DEFAULT 'legacy', "userType" "user_usertype_enum" NOT NULL DEFAULT 'basic-user', "userDetails" jsonb NOT NULL, CONSTRAINT "UQ_78a916df40e02a9deb1c4b75edb" UNIQUE ("username"), CONSTRAINT "UQ_e12875dfb3b1d92d7d7c5377e22" UNIQUE ("email"), CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE UNIQUE INDEX "IDX_e12875dfb3b1d92d7d7c5377e2" ON "user" ("email") `,
    );
    await queryRunner.query(
      `CREATE TABLE "password_reset_links" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "email" character varying NOT NULL, "link" character varying NOT NULL, "isValid" boolean NOT NULL DEFAULT true, CONSTRAINT "PK_cc9575c988e600b1b0b98f16ab7" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_6bb663ec0421d6e8d96a6213c6" ON "password_reset_links" ("email") `,
    );
    await queryRunner.query(
      `CREATE TABLE "token_blacklist" ("id" SERIAL NOT NULL, "token" character varying NOT NULL, "createdTime" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_3e37528d03f0bd5335874afa48d" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "cake" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "name" character varying NOT NULL, "sellerId" uuid NOT NULL, CONSTRAINT "PK_114725dbe66b9db5b3ed3f71070" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "cake_to_ingredients" ("cakeId" uuid NOT NULL, "ingredientId" uuid NOT NULL, "quantity" character varying NOT NULL, "createdDate" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_40e0820f4aaf5b999be3219a226" PRIMARY KEY ("cakeId", "ingredientId"))`,
    );
    await queryRunner.query(
      `CREATE UNIQUE INDEX "IDX_40e0820f4aaf5b999be3219a22" ON "cake_to_ingredients" ("cakeId", "ingredientId") `,
    );
    await queryRunner.query(
      `CREATE TYPE "delivery_status_enum" AS ENUM('preperation', 'dispatch', 'completed')`,
    );
    await queryRunner.query(
      `CREATE TABLE "delivery" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "status" "delivery_status_enum" NOT NULL DEFAULT 'preperation', "address" character varying NOT NULL, "orderId" uuid, CONSTRAINT "REL_b37d51328f9ca210b573b19372" UNIQUE ("orderId"), CONSTRAINT "PK_ffad7bf84e68716cd9af89003b0" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "cake_design_topings_topings" ("cakeDesignId" uuid NOT NULL, "topingsId" uuid NOT NULL, CONSTRAINT "PK_3f8e00c012c062813ff1845d1d0" PRIMARY KEY ("cakeDesignId", "topingsId"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_d804d244ddfc335f5fe1f66de1" ON "cake_design_topings_topings" ("cakeDesignId") `,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_77970c4bb1d6af2009e8686550" ON "cake_design_topings_topings" ("topingsId") `,
    );
    await queryRunner.query(
      `ALTER TABLE "order" ADD CONSTRAINT "FK_caabe91507b3379c7ba73637b84" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "order_cake" ADD CONSTRAINT "FK_2d3d1515adf83d0efa5a56d5823" FOREIGN KEY ("cakeVarietyId") REFERENCES "cake_design"("id") ON DELETE CASCADE ON UPDATE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "order_cake" ADD CONSTRAINT "FK_493c3b235948b29f00989d8cbe9" FOREIGN KEY ("orderId") REFERENCES "order"("id") ON DELETE CASCADE ON UPDATE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "cake_design" ADD CONSTRAINT "FK_0ddb5cbfbad2480a8c026a9af58" FOREIGN KEY ("designerId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "cake_design" ADD CONSTRAINT "FK_0b811458fcc8ed5ae9eeb7a67c5" FOREIGN KEY ("cakeId") REFERENCES "cake"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "group_permission" ADD CONSTRAINT "FK_063bfac21a90aa5103bd6ec08b8" FOREIGN KEY ("groupName") REFERENCES "group"("name") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "group_permission" ADD CONSTRAINT "FK_5b4e81d8139e6df5d858f57e7ce" FOREIGN KEY ("permissionName") REFERENCES "permission"("name") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "user_group" ADD CONSTRAINT "FK_f80e65b06293cb9fa11a8876e15" FOREIGN KEY ("groupName") REFERENCES "group"("name") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "user_group" ADD CONSTRAINT "FK_3d6b372788ab01be58853003c93" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "cake" ADD CONSTRAINT "FK_0a59d9dbc186989d43074efdc5d" FOREIGN KEY ("sellerId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "cake_to_ingredients" ADD CONSTRAINT "FK_4d3481ee98dd5b6eb5ac8fbdf71" FOREIGN KEY ("cakeId") REFERENCES "cake"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "cake_to_ingredients" ADD CONSTRAINT "FK_4bc5dcce61641b92da19b11edcf" FOREIGN KEY ("ingredientId") REFERENCES "ingredients"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "delivery" ADD CONSTRAINT "FK_b37d51328f9ca210b573b19372c" FOREIGN KEY ("orderId") REFERENCES "order"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "cake_design_topings_topings" ADD CONSTRAINT "FK_d804d244ddfc335f5fe1f66de10" FOREIGN KEY ("cakeDesignId") REFERENCES "cake_design"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "cake_design_topings_topings" ADD CONSTRAINT "FK_77970c4bb1d6af2009e8686550e" FOREIGN KEY ("topingsId") REFERENCES "topings"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "cake_design_topings_topings" DROP CONSTRAINT "FK_77970c4bb1d6af2009e8686550e"`,
    );
    await queryRunner.query(
      `ALTER TABLE "cake_design_topings_topings" DROP CONSTRAINT "FK_d804d244ddfc335f5fe1f66de10"`,
    );
    await queryRunner.query(
      `ALTER TABLE "delivery" DROP CONSTRAINT "FK_b37d51328f9ca210b573b19372c"`,
    );
    await queryRunner.query(
      `ALTER TABLE "cake_to_ingredients" DROP CONSTRAINT "FK_4bc5dcce61641b92da19b11edcf"`,
    );
    await queryRunner.query(
      `ALTER TABLE "cake_to_ingredients" DROP CONSTRAINT "FK_4d3481ee98dd5b6eb5ac8fbdf71"`,
    );
    await queryRunner.query(
      `ALTER TABLE "cake" DROP CONSTRAINT "FK_0a59d9dbc186989d43074efdc5d"`,
    );
    await queryRunner.query(
      `ALTER TABLE "user_group" DROP CONSTRAINT "FK_3d6b372788ab01be58853003c93"`,
    );
    await queryRunner.query(
      `ALTER TABLE "user_group" DROP CONSTRAINT "FK_f80e65b06293cb9fa11a8876e15"`,
    );
    await queryRunner.query(
      `ALTER TABLE "group_permission" DROP CONSTRAINT "FK_5b4e81d8139e6df5d858f57e7ce"`,
    );
    await queryRunner.query(
      `ALTER TABLE "group_permission" DROP CONSTRAINT "FK_063bfac21a90aa5103bd6ec08b8"`,
    );
    await queryRunner.query(
      `ALTER TABLE "cake_design" DROP CONSTRAINT "FK_0b811458fcc8ed5ae9eeb7a67c5"`,
    );
    await queryRunner.query(
      `ALTER TABLE "cake_design" DROP CONSTRAINT "FK_0ddb5cbfbad2480a8c026a9af58"`,
    );
    await queryRunner.query(
      `ALTER TABLE "order_cake" DROP CONSTRAINT "FK_493c3b235948b29f00989d8cbe9"`,
    );
    await queryRunner.query(
      `ALTER TABLE "order_cake" DROP CONSTRAINT "FK_2d3d1515adf83d0efa5a56d5823"`,
    );
    await queryRunner.query(
      `ALTER TABLE "order" DROP CONSTRAINT "FK_caabe91507b3379c7ba73637b84"`,
    );
    await queryRunner.query(`DROP INDEX "IDX_77970c4bb1d6af2009e8686550"`);
    await queryRunner.query(`DROP INDEX "IDX_d804d244ddfc335f5fe1f66de1"`);
    await queryRunner.query(`DROP TABLE "cake_design_topings_topings"`);
    await queryRunner.query(`DROP TABLE "delivery"`);
    await queryRunner.query(`DROP TYPE "delivery_status_enum"`);
    await queryRunner.query(`DROP INDEX "IDX_40e0820f4aaf5b999be3219a22"`);
    await queryRunner.query(`DROP TABLE "cake_to_ingredients"`);
    await queryRunner.query(`DROP TABLE "cake"`);
    await queryRunner.query(`DROP TABLE "token_blacklist"`);
    await queryRunner.query(`DROP INDEX "IDX_6bb663ec0421d6e8d96a6213c6"`);
    await queryRunner.query(`DROP TABLE "password_reset_links"`);
    await queryRunner.query(`DROP INDEX "IDX_e12875dfb3b1d92d7d7c5377e2"`);
    await queryRunner.query(`DROP TABLE "user"`);
    await queryRunner.query(`DROP TYPE "user_usertype_enum"`);
    await queryRunner.query(`DROP TYPE "user_signuptype_enum"`);
    await queryRunner.query(`DROP INDEX "IDX_a907a1885bed948d879c278ee3"`);
    await queryRunner.query(`DROP TABLE "user_group"`);
    await queryRunner.query(`DROP INDEX "IDX_8a45300fd825918f3b40195fbd"`);
    await queryRunner.query(`DROP TABLE "group"`);
    await queryRunner.query(`DROP INDEX "IDX_84faca2f810840896c78de1d9a"`);
    await queryRunner.query(`DROP TABLE "group_permission"`);
    await queryRunner.query(`DROP INDEX "IDX_240853a0c3353c25fb12434ad3"`);
    await queryRunner.query(`DROP TABLE "permission"`);
    await queryRunner.query(`DROP INDEX "IDX_63df9c3583a3f70abf04850d07"`);
    await queryRunner.query(`DROP TABLE "cake_design"`);
    await queryRunner.query(`DROP TYPE "cake_design_priceunit_enum"`);
    await queryRunner.query(`DROP TYPE "cake_design_type_enum"`);
    await queryRunner.query(`DROP TABLE "topings"`);
    await queryRunner.query(`DROP INDEX "IDX_e4b26c2272fcb278799fe659e7"`);
    await queryRunner.query(`DROP TABLE "order_cake"`);
    await queryRunner.query(`DROP TABLE "order"`);
    await queryRunner.query(`DROP TYPE "order_status_enum"`);
    await queryRunner.query(`DROP INDEX "IDX_a955029b22ff66ae9fef2e161f"`);
    await queryRunner.query(`DROP TABLE "ingredients"`);
  }
}
