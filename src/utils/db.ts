import 'reflect-metadata';

import { createConnection, DatabaseType, getConnectionOptions } from 'typeorm';

export interface DatabaseConfig {
  type?: DatabaseType;
  database?: string;
  host?: string;
  password?: string;
  username?: string;
  synchronize?: boolean;
  port?: number;
}

export const createDatabaseConnection = async (config?: DatabaseConfig) => {
  const options = await getConnectionOptions();
  Object.assign(options, config);
  return await createConnection(options);
};
