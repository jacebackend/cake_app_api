import { FindOperator, ValueTransformer } from 'typeorm';

export const utcDateTransformer: ValueTransformer = {
  from: (dateString: string) => new Date(dateString),
  to: (source: Date | FindOperator<any>) => {
    if (source instanceof Date) {
      return toDateString(source);
    }
    return source;
  },
};

export const toDateString = (value: Date) => value.toISOString().split('T')[0];
