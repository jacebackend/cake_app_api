import { Class } from '@foal/core';
import { classToPlain, plainToClass } from 'class-transformer';

export const marshallResult = (result: any, marshaller: Class) =>
  classToPlain(plainToClass(marshaller, classToPlain(result)));
