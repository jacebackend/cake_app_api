# Cake application api

This is the api consumed by the cake application

### API Docs

[docs](https://jacebackend-cake-app-api-staging.35.188.146.141.nip.io/docs)

**Or just use postman**

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/3a2fb17834e1e3531699)

### Pre-requisites

Ensure the following have been installed:

* Git
* Node
* Yarn
* Docker
* FoalTs

### Setupv

* run `docker volume create pgDatabase`, This creates a volume to persist your changes between builds

#### Authors

[Paul Rimiru](https://github.com/PaulKariukiRimiru)
**Edwin Kimaita**
