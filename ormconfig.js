const { Config } = require('@foal/core');

module.exports = {
  type: 'postgres',
  url: Config.get('database.url'),
  entities: ['build/app/entities/**/*.entity.js'],
  migrations: ['build/migrations/*.js'],
  seeds: ['build/**/*.seed.js'],
  factories: ['build/database/factories/*.js'],
  cli: {
    entitiesDir: 'src/app/entities/**/*.entity.ts',
    migrationsDir: 'src/migrations',
  },
  logging: false,
  autoSchemaSync: true,
};
